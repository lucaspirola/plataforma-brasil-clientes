var markdown = require('markdown');
var API = markdown.markdown;

module.exports = function (input) {
  return API.toHTMLTree(input);
};
