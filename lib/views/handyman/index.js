var m = require('mithril');
var badge_discussion = require('views/common/badge_discussion');
var theme_details = require('views/common/theme_details');
var handy_link = require('views/handyman/box_link');

var handyman = function (ctrl) {
	var list_handyman = [];
	ctrl.list_handyman().forEach(function(content, k) {
		list_handyman.push(handy_link(ctrl, content, 'handyman'));
	});
	return [
		theme_details('compact', ctrl.featured_discussion().theme),
		m('.bd-box-post.container', badge_discussion(ctrl.featured_discussion(), 'handyman', true, true, true, true)),
		m('.pure-g.container',[
			m('.pure-u-22-24', [
				m('.pure-u-sm-1-5'),
				m('.pure-u-sm-4-5.pure-u-1', list_handyman)
			])
		])
	];
};

module.exports = handyman;
