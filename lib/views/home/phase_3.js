var m = require('mithril');
var challenges = require('views/home/phase_3_challenges');
var feature_box= require('views/common/feature_box');
var creditos_foto = require('views/common/creditos_foto');
var module_phases = require('views/home/phase_3_box_phases');

var loginModule = require('controllers/login');

module.exports = function (ctrl) {
    return [
        m('.hero.home', { style: { 'background-image': 'url('+ctrl.bg()+')' } }, [
            m('.container', [
                m('.pure-g', feature_box(
                    m('img.svg.logo-square[src=img/logo.svg]'),
                    m('h1', ctrl.titulo())
                ))
            ]),
            creditos_foto()
        ]),
        m('.pure-g',
            m('.pure-u-1',
                m('.entenda-box', [
                  m('span', 'veja como foi'),
                  m('i.icon-triangle-down')
                ])
             )
         ),
        module_phases(ctrl),
        m('.challenges.container', [
            challenges(ctrl)
        ]),
        m('br')
    ];
};

