var m = require('mithril');
var button = require('views/common/button');

var badge_phase = function (ctrl,name,step,date_aux, text_aux,icon) {
	return [
		m('.pure-u-1.pure-u-sm-1-3.center-content.box-phases',[
			m('.feature-image', [
	            m('img.svg[src=img/illustration/'+step+'.svg][width=150]'),
	        ]),
	        m('h1.icon-badge-phase.pure-visible-only-sm'+icon),
			m('h2',name),
			m('h3.pure-visible-only-sm',date_aux),
			m('h3.pure-u-1.pure-visible-only-sm',text_aux)
		])
	];
};
var box_phases = function (ctrl) {
	return [
		badge_phase(ctrl, ctrl.fase_1_nome(),'step1',ctrl.fase_1_duracao(),'','.icon-controller-record'),
		badge_phase(ctrl, ctrl.fase_2_nome(),'step2',ctrl.fase_2_duracao(),'','.icon-controller-record'),
		badge_phase(ctrl, ctrl.fase_3_nome(),'step3',ctrl.fase_3_duracao(),'Estamos aqui','.icon-location-pin.red-color')
	];
};
var phase_marker_dates = function(ctrl) {
	return [
		m('.container.pure-u-1.box-marker-dates.pure-hidden-sm',[
			m('.pure-u-1.pure-u-sm-1-3.center-content',[
				m('p',[
					m('h3',ctrl.fase_1_duracao())
				]),
				button('icon-line-graph','ver comentários','da fase 1','comentarios-fase-1')
			]),
			m('.pure-u-1.pure-u-sm-1-3.center-content',[
				m('p',[
					m('h3',ctrl.fase_2_duracao()),
				]),
				button('icon-discussao','ver comentários','da fase 2','comentarios-fase-2')
			]),
			m('.pure-u-1.pure-u-sm-1-3.center-content',[
				m('p',[
					m('h3',ctrl.fase_3_duracao()),
					m('h3','Estamos aqui!')
				]),
				m('i.icon-arrow-down.icon-xxlarge')
			])
		])
	];
};
var phase_marker = function(ctrl) {
	return [
		m('.container.pure-u-1.box-marker',[
			m('.pure-u-1.pure-u-sm-1-3.center-content',[
				m('h1.icon-controller-record.pure-hidden-sm')
			]),
			m('.pure-u-1.pure-u-sm-1-3.center-content',[
				m('h1.icon-controller-record.pure-hidden-sm')
			]),
			m('.pure-u-1.pure-u-sm-1-3.center-content',[
				m('h1.icon-location-pin.red-color.pure-hidden-sm')
			]),
			m('hr.phase-bar')
		])
	];
};

var module_phases = function (ctrl) {
	return [
		m('.container.pure-g.module-phases',[
			box_phases(ctrl),
			phase_marker(ctrl),
			phase_marker_dates(ctrl)
		])
	];
};

module.exports = module_phases;
