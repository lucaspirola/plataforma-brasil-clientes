var m = require('mithril');
var new_step = function (ctrl) {
    return [
    m('.step.three.pure-g', [
        m('.feature-image.pure-u-1.pure-u-sm-12-24', [
            m('img.pure-img.svg[src=img/illustration/step3.svg]'),
        ]),
        m('.feature-text.pure-u-1.pure-u-sm-10-24', [
            m('.position.separator.pure-u-1-5', [
                m('h2.number.icon-relatoria'),
            ]),
            m('.title.pure-u-4-5', [
                m('h2', ctrl.fase_3_nome()),
                m('h3', ctrl.fase_3_duracao())
            ]),
            m('p.description', ctrl.fase_3_descricao())
        ])
    ])
];
};

module.exports = new_step;
