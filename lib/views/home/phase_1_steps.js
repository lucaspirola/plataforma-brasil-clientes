var m = require('mithril');
var steps = function () {
    return [
    m('.step.one.pure-g', [
        m('.feature-image.pure-u-1.pure-u-sm-12-24', [
            m('img.pure-img.svg[src=img/illustration/step1.svg]'),
        ]),
        m('.feature-text.pure-u-1.pure-u-sm-10-24', [
            m('.position.separator.pure-u-1-5', [
                m('h2.number', '01')
            ]),
            m('.title.pure-u-4-5', [
                m('h2', 'Priorizar Temáticas'),
                m('h3', 'de maio a junho'),
            ]),
            m('p.description', 'A consulta da Plataforma Brasil é composta por três fases. Na primeira fase deste primeiro ciclo, cujo tema é ‘Reforma Política do Século 21’, serão sugeridos os subtemas mais urgentes para serem tratados ao longo do processo. Alguns deles já serão sugeridos, mas também é possível sugerir outros e todos passarão por uma priorização. Os mais votados serão os debatidos na próxima fase.'),
            m('a.pure-button.button-xlarge[href=/fase-1-votar]', {config: m.route}, 'Participar')
        ])
    ]),
    m('.step.two.pure-g', [
        m('.feature-image.pure-u-1.pure-u-sm-12-24', [
            m('img.pure-img.svg[src=img/illustration/step2.svg]'),
        ]),
        m('.feature-text.pure-u-1.pure-u-sm-10-24', [
            m('.position.separator.pure-u-1-5', [
                m('h2.number', '02')
            ]),
            m('.title.pure-u-4-5', [
                m('h2', 'Propor Soluções'),
                m('h3', 'de junho a agosto')
            ]),
            m('p.description', 'Na segunda fase, vamos discutir os subtemas mais votados. O objetivo é promover um debate informado e participativo, onde usuários podem colaborar por meio de suas propostas.')
        ])
    ]),
    m('.step.three.pure-g', [
        m('.feature-image.pure-u-1.pure-u-sm-12-24', [
            m('img.pure-img.svg[src=img/illustration/step3.svg]'),
        ]),
        m('.feature-text.pure-u-1.pure-u-sm-10-24', [
            m('.position.separator.pure-u-1-5', [
                m('h2.number', '03'),
            ]),
            m('.title.pure-u-4-5', [
                m('h2', 'Compilar Propostas'),
                m('h3', 'de agosto a setembro')
            ]),
            m('p.description', 'Quando a segunda fase se encerrar, chegamos à terceira fase, onde as propostas recebidas serão compiladas de forma transparente e representativa em documentos que serão entregues aos atores relevantes ligados à elaboração e execução da política pública debatida.')
        ])
    ])
];
};

module.exports = steps;
