var m = require('mithril');
var steps = require('views/home/phase_2_steps');
var challenges = require('views/home/phase_2_challenges');
var discussion = require('views/home/phase_2_discussion');
var feature_box= require('views/common/feature_box');
var creditos_foto = require('views/common/creditos_foto');
var module_phases = require('views/home/phase_2_box_phases');

var loginModule = require('controllers/login');

module.exports = function (ctrl) {
    return [
        m('.hero.home', { style: { 'background-image': 'url('+ctrl.bg()+')' } }, [
            m('.container', [
                m('.pure-g', feature_box(
                    m('img.svg.logo-square[src=img/logo.svg]'),
                    m('h1', ctrl.titulo())
                ))
            ]),
            creditos_foto()
        ]),
        m('.pure-g',
            m('.pure-u-1',
                m('.entenda-box', [
                  m('span', 'entenda'),
                  m('i.icon-triangle-down')
                ])
             )
         ),
        module_phases(ctrl),
        m('.challenges.container', [
            challenges(ctrl)
        ]),
        m('.discussions.container', [
            discussion(ctrl)
        ]),
        m('.steps.container', [
            steps(ctrl)
        ]),
        loginModule.view(ctrl.login_partial)

    ];
};
