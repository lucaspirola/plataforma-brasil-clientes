var m = require('mithril');
var baloon_bottom = require('views/common/baloon_bottom');
var button = require('views/common/button');

function validateUrl(url) {
	var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
	var regex = new RegExp(expression);
	return url.match(regex);
}
var combo_button = function(theme_id, download_url, download_click, text_button) {
	return [
		m('.pure-g',[
			m('.pure-u-1',
				m('a.pure-button.full[href='+download_url+']',
				{ onclick: download_click.bind(this, download_url) },
				[
					m('i.icon-relatorios-2'),
					text_button
				])
			),
			m('.pure-u-1.pure-u-sm-1-3',
				button('icon-line-graph','ver comentários','da fase 1','comentarios-fase-1')
			),
			m('.pure-u-1.pure-u-sm-1-3',
				button('icon-discussao','ver comentários','da fase 2','comentarios-fase-2?theme_id='+theme_id)
			),
			m('.pure-u-1.pure-u-sm-1-3',
				button('icon-estatisticas','ver perfil de','contribuições','relatorios-fase-3')
			)
		])
	];
};

var download = function (url, redirect) {
	if (validateUrl(url)) { window.open(url); m.route(redirect); }
	return false;
};

var download_success_tema_1 = function(url) {
	return download(url, '/download-relatorio-tema-1');
};
var download_success_tema_2 = function(url) {
	return download(url, '/download-relatorio-tema-2');
};
var download_success_tema_3 = function(url) {
	return download(url, '/download-relatorio-tema-3');
};
var download_success_tema_4 = function(url) {
	return download(url, '/download-relatorio-tema-4');
};
var download_success_tema_5 = function(url) {
	return download(url, '/download-relatorio-tema-5');
};


var item_tema = function(data, k, title, text, link, download_url, download_click, text_button) {
	return m('li', [
		m('input.accordion-select[type=radio][name=select]'+(k===0 ? '[checked]' : '')),
		m('.accordion-title', [
			m('span.accordion-number', ("0" + data.position).substr(-2,2)),
			m('.accordion-subtitle', data.name)
		]),
		m('.accordion-content.center-content', [
			m('h1',title),
			m('p', text),
			( (link === '' || link === undefined) ? '' :
				m('a.read-more[href="'+link+'"]','Leia mais no blog')
			),
			combo_button(data.id, download_url, download_click, text_button)
		]),
		m('.accordion-separator')
	]);
};

var box_menu = function (ctrl) {
	var arrayTitle = [
		ctrl.page_theme_01_title_relatoria_fase_2(),
		ctrl.page_theme_02_title_relatoria_fase_2(),
		ctrl.page_theme_03_title_relatoria_fase_2(),
		ctrl.page_theme_04_title_relatoria_fase_2(),
		ctrl.page_theme_05_title_relatoria_fase_2(),
	];
	var arrayText = [
		ctrl.page_theme_01_intro_relatoria_fase_2(),
		ctrl.page_theme_02_intro_relatoria_fase_2(),
		ctrl.page_theme_03_intro_relatoria_fase_2(),
		ctrl.page_theme_04_intro_relatoria_fase_2(),
		ctrl.page_theme_05_intro_relatoria_fase_2(),
	];
	var arrayLink = [
		ctrl.page_theme_01_link_relatoria_fase_2(),
		ctrl.page_theme_02_link_relatoria_fase_2(),
		ctrl.page_theme_03_link_relatoria_fase_2(),
		ctrl.page_theme_04_link_relatoria_fase_2(),
		ctrl.page_theme_05_link_relatoria_fase_2(),
	];
	var arrayDownload = [
		ctrl.page_url_tema_1_relatoria_fase_3(),
		ctrl.page_url_tema_2_relatoria_fase_3(),
		ctrl.page_url_tema_3_relatoria_fase_3(),
		ctrl.page_url_tema_4_relatoria_fase_3(),
		ctrl.page_url_tema_5_relatoria_fase_3(),
	];
	var arrayDownloadClick = [
		download_success_tema_1,
		download_success_tema_2,
		download_success_tema_3,
		download_success_tema_4,
		download_success_tema_5
	];
	var arrayTextButton = [
		'Relatório final Tema 1 : Decisões Políticas Transparentes',
		'Relatório final Tema 2 : Financiamento de Campanhas Eleitorais',
		'Relatório final Tema 3 : Ampliação dos Espaços de Consulta à Sociedade',
		'Relatório final Tema 4 : Fiscalização e Transparência das Doações para as Campanhas Eleitorais',
		'Relatório final Tema 5 : Participação Cidadã na Internet'
	];
	return [
		m('.accordion.ac-phase3', [
			m('ul', function () {
				var options = [];
				var arrayPosition = 0;
				ctrl.themes().forEach(function (data, k) {
					options.push(item_tema(data, k, arrayTitle[arrayPosition], arrayText[arrayPosition], arrayLink[arrayPosition], arrayDownload[arrayPosition], arrayDownloadClick[arrayPosition], arrayTextButton[arrayPosition]));
					arrayPosition++;
				});
				return options;
			}())
		])
	];
};

// http://accordionslider.com/
var box_challenge = function (ctrl) {
	return [
		m('.accordion-block-title',[
			m('h2.center-content', 'ACOMPANHE O PROCESSO DE RELATORIA PARA CADA UM DOS TEMAS DISCUTIDOS:')
		]),
		m('.container',[
			box_menu(ctrl)
		])
	];
};

module.exports = box_challenge;
