var m = require('mithril');

var item_tema = function(data, k) {
    return m('li', [
		m('input.accordion-select[type=radio][name=select]'+(k===0 ? '[checked]' : '')),
		m('.accordion-title', [
			m('span', ("0" + data.position).substr(-2,2))
		]),
		m('.accordion-content', [
			m('p', data.name),
			m('a.pure-button.big[href=/fase-2-discutir-tema/'+data.id+']', {config: m.route}, 'responder')
		]),
		m('.accordion-separator')
	]);
};

var choose_themes = function(ctrl) {
	return m('.accordion', [
		m('ul', function () {
			var options = [];
			ctrl.themes().forEach(function (data, k) {
				options.push(item_tema(data, k));
			});
			return options;
		}())
	]);
};

module.exports = choose_themes;
