var m = require('mithril');


var bd_filter = function(ctrl) {
	var recent, hot = false;
	if (ctrl.order() === 'mais-novas') {
		recent = true;
	} else {
		hot = true;
	}
	return [
		m('a.filter-item'+(hot ? '.selected' : '')+'[href=/fase-2-discutir-tema/'+
			ctrl.theme_id()+'?ordenar=mais-quentes]', {
				config: m.route
			}, [
				m('i.icon-quente'),
				m('span','MAIS QUENTES')
			]
		),
		m('a.filter-item'+(recent ? '.selected' : '')+'[href=/fase-2-discutir-tema/'+
			ctrl.theme_id()+'?ordenar=mais-novas]', {
				config: m.route
			}, [
				m('i.icon-relogio'),
				m('span','MAIS NOVAS')
			]
		)
		// m('a[href=#]', [
		// 	m('span','FILTROS'),
		// 	m('i.icon-filtrar')
		// ])
	];
};

module.exports = bd_filter;
