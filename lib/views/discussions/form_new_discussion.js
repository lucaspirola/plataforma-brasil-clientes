var m = require('mithril');
var filter_categories = require('views/discussions/filter_categories');
var list_categories = require('views/common/list_categories_from_theme');
var badge_user = require('views/common/badge_user');
var badge_feedback = require('views/common/badge_feedback');
var btn_new_comment = function(ctrl) {
	return [
				m('.discussion-filter-item',[
					m('a.pure-button.btn-new-discussion'+
					  (ctrl.is_visible_form_discussion() ? '.selected' : '')+
					  '[href=/#]', { onclick: ctrl.toggle_form_discussion.bind(this, ctrl)}, [
						m('i.icon-comentar-discussao'),
						m('span','Dê a sua resposta'),
					])
				]),
				m('.new-discussion'+(ctrl.is_visible_form_discussion() ? '' : '[hidden]'), 
					(ctrl.is_user_loggedin() ? 
						m('.pure-form', [
							m('.profile-info.pure-u-1-5',[
								badge_user(ctrl.current_user(), 50)
							]),
							m('.comment-title.pure-u-3-5',[
								m('input[type=text][placeholder=Título em destaque (opcional)].txt-title', 
								  {onchange: m.withAttr('value', ctrl.model.title),  value: ctrl.model.title()}),
								m('textarea[placeholder=Espaço aberto para você apresentar sua opnião.].txt-opnion', 
								  {onchange: m.withAttr('value', ctrl.model.description),  value: ctrl.model.description()}),
								m('p.comment-label','selecione as categorias relacionadas'),
								m('.comment-tag-name',[
									m('.discussion-categories', list_categories(ctrl, ctrl.toggle_checkbox)),
								]),
								badge_feedback(ctrl.model.message),
								m('.actions', [
									m('a.save-discussion.pure-button[href=#]', { onclick: ctrl.save_discussion.bind(this, ctrl) }, 'publicar'),
									m('a.cancelar.cancel-discussion.pure-button[href=#]', { onclick: ctrl.toggle_form_discussion.bind(this, ctrl)}, 'fechar')
								])
							])
						])
					: m('p.not-loggedin', [
						'Quer participar da discussão? Antes você precisa ',
						m('a[href=/entrar?back_to='+m.route()+']', {config:m.route}, 'fazer o login.')
					]))
				)
	];
};
module.exports = btn_new_comment;
