var m = require('mithril');
var btn_new_comment = require('views/discussions/form_new_discussion');
var badge_discussion = require('views/common/badge_discussion');
var theme_details = require('views/common/theme_details');
var filter_categories = require('views/discussions/filter_categories');
var bd_filter = require('views/discussions/filter_options');
var loading = require('views/common/loading');
var box_discussion = function (ctrl) {

	var PAGE_SIZE = 10;
	var range = 0 + '-' + PAGE_SIZE;

	ctrl.updateListAfterRequest = function (data) {
		Array.prototype.push.apply(ctrl.list_discussions(), data);
		//this.totalRecords.
	};


	var view_discussions = m.prop([]);

	if (ctrl.list_discussions() !== null) {
		var currentTotalDiscussions = ctrl.list_discussions().length;

		if (currentTotalDiscussions > 0) {
			var offset = currentTotalDiscussions + PAGE_SIZE;
			range = currentTotalDiscussions + '-' + offset;
		}

		ctrl.list_discussions().forEach(function(content, k) {
			view_discussions().push(badge_discussion(content, 'Discussions', true, true, true, false));
		});
	} else {
		view_discussions().push([m('.no-results.pure-u-4-5', [
			m('p', 'Não existem discussões para este tema ainda, comece uma!')
		])]);
	}
    return [
		theme_details('featured', ctrl.theme_active()),
		m('.box-discussions.container',
			m('.bd-filter.pure-u-1',[
		    	m('.discussion-filter-comment',btn_new_comment(ctrl))
		    ]),
			m('.bd-box-post', [
				m('.pure-u-sm-2-5.pure-u-1.filter-toolbar', bd_filter(ctrl)),
				m('.pure-u-sm-3-5.pure-u-1.filter-toolbar.pure-hidden-sm', filter_categories(ctrl))
			]),
			(ctrl.isLoading() ? loading() : view_discussions())
		),
		(  (ctrl.list_discussions() !== null) && (ctrl.list_discussions().length < ctrl.total_records()) ?
			m('.center-content',
				m('a.pure-input-1.pure-button.btn-send-icon#btn-read-more[href=#]', 
					{ onclick: ctrl.get_more_discussions.bind(this, ctrl, range) },
					'mais comentários'
				)
			)
		: '')
	];
};

module.exports = box_discussion;
