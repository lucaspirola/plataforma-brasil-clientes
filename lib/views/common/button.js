var m = require('mithril');
var button = function (icon,text_small,text_big,link) {
    return [
            m('a.button-item.pure-button[href=/'+link+']',{config:m.route},[
                m('i.'+icon),
                m('span.small',text_small),
                m('span.big',text_big)
            ])
    ];
};

module.exports = button;
