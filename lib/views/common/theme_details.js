var m = require('mithril');

var theme_details = function (extraClass, content) {
	return m('.pl-megabanner.' + extraClass,[
			m('.container', [
				m('.line-number',("0" + content.position).substr(-2,2)),
				m('.featured-box',[
					m('h1', m('a[href=/comentarios-fase-2/?theme_id=' + content.id + ']', {config:m.route}, content.name)),
					m('span','Responda até 21/8')
				])
			])
		]);
};

module.exports = theme_details;
