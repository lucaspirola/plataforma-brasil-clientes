var m = require('mithril');
var baloon_bottom = function () {
    return [
        m('svg.baloon-bottom[viewBox=0 0 960 72][preserveAspectRatio=xMinYMin meet][version=1.1][xmlns=http://www.w3.org/2000/svg][xmlns:xlink=http://www.w3.org/1999/xlink]',
            m('g[stroke=none][stroke-width=1][fill=none][fill-rule=evenodd]',
                m('path[d=M960,0.8 L0,0.8 L0,25.0091691 L43.5546306,25.0091691 L43.5546306,71.8 L79.3173777,25.0091691 L960,25.0091691 L960,0.8 L960,0.8 Z]')
            )
         )
    ];
};

module.exports = baloon_bottom;
