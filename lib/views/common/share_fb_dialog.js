var m = require('mithril');
var c = require('config');

var share = function(description, caption, url, title) {
	configCompartilhar = {
		method: 'feed',
		description: description,
		name: title,
		caption: caption,
		picture: c.getHost() + '/img/logo-facebook.png',
		link: c.getHost() + url,

	};
	if (window.cordova) {
		facebookConnectPlugin.showDialog(configCompartilhar, function(response){});
	} else {
		FB.ui(configCompartilhar, function(response){});
	}
};

module.exports = share;
