var m = require('mithril');

var divisor = function(icon,count,text) {
	return [
		m('.feature-text.pure-g', [
	        m('.position.separator.pure-u-sm-1-4.pure-u-1-4', [
	            m('h1.number.'+icon,count),
	        ]),
	        m('.title.pure-u-sm-3-4.pure-u-5-8', [
	            m('h2', text),
	        ])
	    ])
    ];
};
module.exports = divisor;