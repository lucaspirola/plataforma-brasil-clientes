var m = require('mithril');
var open_external_link = require('views/common/open_external_link');
var creditos_foto = function () {
    return m('.creditos', [
            m('a[href=https://www.flickr.com/photos/midianinja/9124087670/in/album-72157634297891294/]', {onclick: open_external_link}, '(CC BY-SA) Alguns direitos reservados')
        ]);
};

module.exports = creditos_foto;
