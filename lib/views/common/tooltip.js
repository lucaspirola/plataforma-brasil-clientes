var m = require('mithril');

var exp =  function(content_tip){
	return m( '.tooltip', {
			style : {
				top  : content_tip.event.y + 40 + 'px',
				left : content_tip.event.x + 'px'
			}
		}, content_tip.content );
};

module.exports = exp;
