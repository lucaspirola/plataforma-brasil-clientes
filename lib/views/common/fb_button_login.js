var m = require('mithril');
var fb_button_login = function () {

    var ctrl = {};
    ctrl.login= function(e) {

        ctrl.permissions = ['public_profile','email','friend_list'];
        if (window.cordova) {
            facebookConnectPlugin.login(ctrl.permissions, ctrl.parseLoginSuccess, ctrl.catchLoginError);
        } else {
            var permissionObj = {};
            if (ctrl.permissions && ctrl.permissions.length > 0) {
                permissionObj.scope = ctrl.permissions.toString();
            }
            FB.login(ctrl.parseLoginSuccess, permissionObj);
        }
    };

    ctrl.parseLoginFail = function (response) {
        console.log(response);
    };

    ctrl.parseLoginSuccess = function (response) {
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
             m.route('/abrir-conta/'+response.authResponse.accessToken+'?back_to='+m.route.param('back_to'));
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
        } else {
             // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
        }
    };

    return [
        m ('a.menu-item.login-facebook[href=#]', {onclick: ctrl.login}, [
            m('i.icon-facebook'),
            m('span.small', 'login ou cadastro'),
            m('span.big', 'facebook')
        ])
    ];
};

module.exports = fb_button_login;
