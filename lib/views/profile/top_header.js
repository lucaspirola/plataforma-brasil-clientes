var m = require('mithril');
var badge_user = require('views/common/badge_user');

var top_header = function(ctrl,user,name,depto) {
	return [
		m('.pure-g.profile-info-notification.pure-hidden-sm',[
			m('.profile-info.pure-u-5-24',[
				badge_user(ctrl.current_user(), 200)
			]),
			m('.pure-u-19-24',[
				m('h1.profile-name',name)
			])
		])
	];
};
module.exports = top_header;
