var m = require('mithril');
var badge_user = require('views/common/badge_user');
var form = function(ctrl) {
    var profile=[];

    profile.sector = ctrl.vm.sector();
    
    profile.photo_url = ctrl.vm.photo_url();

    return [
        m('.pure-g', [
            m(".pure-u-1.pure-u-sm-8-24.center-content", [
                badge_user({photo_url:ctrl.vm.photo_url(), nome: ctrl.vm.nome(), sector: ctrl.vm.sector()}, 200),
                ctrl.vm.facebookSignup ? '' :
                m(".profile-btn-bg", [
                    m('input[type=file][accept="image/*"][name=profile-picture]', {
                        onchange: m.withAttr('files', ctrl.vm.profilePicture)
                    })
                ]),
                m('.pure-u-1', ctrl.error())
            ]),
            m('.pure-u-1.pure-u-sm-16-24', [
                m('.pure-g', [
                    m('.pure-u-14-24', [
                        m('input[type=text][name=nome][placeholder=Nome]', {
                            required: true,
                            onchange: m.withAttr('value', ctrl.vm.nome),
                            value: ctrl.vm.nome()
                        })
                    ]),
                    m('.pure-u-10-24', [
                        m('input[type=text][name=data_nascimento][placeholder=Nascimento (dd/mm/aaaa)]', {
                            onchange: m.withAttr('value', ctrl.vm.data_nascimento),
                            value: ctrl.vm.data_nascimento()
                        })
                    ])
                ]),
                m('.pure-g', {
                    style: ctrl.vm.facebookSignup ? 'display: none' : 'display: block'
                }, [
                    m('.pure-u-1-2', [
                        m('input[type=email][name=email][placeholder=Email]', {
                            required: ctrl.vm.facebookSignup ? false : true,
                            onchange: m.withAttr('value', ctrl.vm.email),
                            value: ctrl.vm.email()
                        })
                    ]),
                    m('.pure-u-1-2', [
                        m('input[type=password][name=password][placeholder=Senha]', {
                            required: ctrl.vm.facebookSignup  ? false : true,
                            onchange: m.withAttr('value', ctrl.vm.password),
                            value: ctrl.vm.password()
                        })
                    ])
                ]),
                m('.pure-g', {
                    style: ctrl.vm.facebookSignup ? 'display: block' : 'display: none'
                }, [
                    m('.pure-u-1', [
                        m('input[type=email][name=email][placeholder=Email]', {
                            required: ctrl.vm.facebookSignup ? true : false,
                            onchange: m.withAttr('value', ctrl.vm.email),
                            value: ctrl.vm.email()
                        })
                    ])
                ]),
                m('.pure-g', [
                    m('.pure-u-1-2', [
                        m('input[type=text][name=ocupacao][placeholder=Profissão]', {
                            onchange: m.withAttr('value', ctrl.vm.ocupacao),
                            value: ctrl.vm.ocupacao()
                        }),
                    ]),
                    m('.pure-u-1-2', [
                        m('input[type=text][name=genero][placeholder=Sexo]', {
                            onchange: m.withAttr('value', ctrl.vm.genero),
                            value: ctrl.vm.genero()
                        }),
                    ]),
                ]),
                m('.pure-g', [
                    m('.pure-u-1-3', [
                        m('select[name=estado]', {
                            required: true,
                            value: ctrl.vm.selectedState(),
                            disabled: ctrl.vm.stateDisabled,
                            onchange: function() {
                                ctrl.vm.changeState(this.value);
                            }
                        }, [
                            ctrl.vm.states().map(function(value) {
                                return m('option', {
                                    value: value.id
                                }, value.name);
                            })
                        ])
                    ]),
                    m('.pure-u-2-3', [
                        m('select[name=cidade]', {
                            onchange: m.withAttr('value', ctrl.vm.selectedCitie),
                            value: ctrl.vm.selectedCitie(),
                            disabled: ctrl.vm.citiesDisabled
                        }, [
                            ctrl.vm.cities().map(function(value) {
                                return m('option', {
                                    value: value
                                }, value);
                            })
                        ])
                    ]),
                    m('.pure-u-1-2', {
                        style: ctrl.vm.facebookSignup ? 'display: none' : 'display: block'
                    }, [
                        m('input[type=text][name=cpf][readonly][placeholder=CPF]', {
                            required: ctrl.vm.facebookSignup ? false : true,
                            onchange: m.withAttr('value', ctrl.vm.cpf),
                            value: ctrl.vm.cpf()
                        })
                    ])
                ]),
                m('.pure-g',
                    m('.pure-u-1', [
                        m('textarea[name=descricao][placeholder=Fale um pouco sobre você...][row=5][cols=20]', {
                            value: ctrl.vm.descricao(),
                            onkeypress: ctrl.contaCaracteres.bind(ctrl, this)
                        }),
                        m('p.char-count', 'Caracteres restantes: ' + ctrl.caracteresRestantes()),
                    ]))
            ]),
        ])
    ];
};

module.exports = form;
