var m = require('mithril');
var new_suggest = function (ctrl) {
    return [
    m('.pure-form.sugestao-box', [
        m('textarea[name=texto-nova-sugestao][placeholder=Insira sua sugestão]', {
            onchange: m.withAttr('value', ctrl.contentNovaOpcao),
            value: ctrl.contentNovaOpcao()
        }),
        ctrl.statusNovaOpcao(),
        m('button.pure-button.cancelar[href=#]', {
            onclick: ctrl.fecharNovaOpcao.bind(ctrl, this)
        }, 'Fechar'),
        m('button.pure-button', {
            onclick: ctrl.salvarNovaOpcao.bind(ctrl, this)
        }, 'Enviar')
    ])
    ];
};

module.exports = new_suggest;
