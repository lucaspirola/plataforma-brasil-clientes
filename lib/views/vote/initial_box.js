var m = require('mithril');
var new_suggest = require('views/vote/new_suggest');
var open_external_link = require('views/common/open_external_link');
var initial_box = function (ctrl) {
    return [
        m('.pure-g', [
            m('.pure-u-1.pure-u-sm-4-24', [
                m('.pure-u-1-2.pure-u-sm-1', [
                    m('a.convidar-amigos.secondary-action[href=#]', {
                        onclick: ctrl.abrirAjudaAmigo.bind(ctrl, this)
                    }, [
                        m('span.image', m('img[src=img/icon/email.svg]')),
                        m('span.label', [
                            'pedir ajuda',
                            m('br'),
                            'a um amigo'
                        ])
                    ])
                ]),
                m('.pure-u-1-2.pure-u-sm-1', [
                    m('a.acessar-bibliteca.secondary-action[href=http://www.itsrio.org/civviki/index.php?title=Categoria:Reforma_Pol%C3%ADtica_do_S%C3%A9culo_XXI][target=_blank]', {onclick: open_external_link }, [
                        m('span.image', m('img[src=img/icon/biblioteca.svg]')),
                        m('span.label', [
                            'Quero saber',
                            m('br'),
                            'o que significa'
                        ])
                    ])
                ]),
            ]),
            m('.pure-u-1-2.pure-u-sm-8-24', [
                 m('.option.left-choice', {
                         id: ctrl.options()['left-choice-id'],
                        onclick: m.withAttr('id', ctrl.sendVote)
                    },
                    m('p.label', 'quero priorizar:'),
                    m('p.description', ctrl.options()['left-choice-text']),
                    m('.pure-button', 'escolha este'),
                    m('.or', 'ou')
                )
            ]),
            m('.pure-u-1-2.pure-u-sm-8-24', [
                m('.option.right-choice', {
                        id: ctrl.options()['right-choice-id'],
                        onclick: m.withAttr('id', ctrl.sendVote)
                    },
                    m('p.label', 'quero priorizar:'),
                    m('p.description', ctrl.options()['right-choice-text']),
                    m('.pure-button', 'escolha este')
                )
            ]),
        ]),
        m('.pure-g', [
            m('.pure-u-4-24.pure-hidden-sm'),
            m('.pure-u-3-5.pure-u-sm-10-24.enviar-sugestao.extra-action', [
                m('a[href="#"]', {
                    onclick: ctrl.abrirNovaOpcao.bind(ctrl, this)
                }, [
                    m('img[src=img/icon/votar-exclamacao.svg]'),
                    m('span', 'PRIORIZAR OUTRO TEMA')
                ]),
                ctrl.abertoNovaOpcao() ? new_suggest(ctrl) : ''
            ]),
            m('.pure-u-2-5.pure-u-sm-6-24.pular-opcao.extra-action', [
                m('a[href="#"]', {
                    onclick: ctrl.pularOpcoes.bind(ctrl, this)
                }, [
                    m('img[src=img/icon/votar-interrogacao.svg]'),
                    m('span', 'PULAR OPÇÕES')
                ])
            ]),
        ]),
    ];
};

module.exports = initial_box;
