var m = require('mithril');
var badge_user = require('views/common/badge_user');
var badge_feedback = require('views/common/badge_feedback');
var box_new_comment = function(ctrl) {

	return [
		m('.box-new-comment.pure-form',[
			(ctrl.is_user_loggedin() ? 
				m('.pure-form.pure-u-22-24', [
					m('.profile-info.pure-u-1-5.pure-u-sm-1-5',[
						badge_user(ctrl.current_user(), 50)
					]),
					m('.pure-u-3-5.pure-u-sm-2-3.form-new-comment', [
						m('input.pure-input-1[placeholder=Comentar][type=text]', {onchange: m.withAttr('value', ctrl.model.description),  value: ctrl.model.description()}),
						badge_feedback(ctrl.model.message)
					]),
					m('.pure-u-1-5.pure-u-sm-1-8',[
						m('a.pure-input-1.pure-button.pure-hidden-sm.btn-send-icon[href=#]', { onclick: ctrl.save_comment.bind(this, ctrl) }, 'enviar'),
						m('a.pure-input-1.pure-button.pure-visible-only-sm.icon-forward.btn-send-icon[href=#]', { onclick: ctrl.save_comment.bind(this, ctrl) }, '')
					])
				])
			: m('p.not-loggedin', [
				'Para mostrar sua opnião através dos comentários, você precisa ',
				m('a[href=/entrar?back_to='+m.route()+']', {config:m.route}, 'fazer login'),
				' antes.'
			]))
		])
	];
};

module.exports = box_new_comment;

// icon-align-right
// icon-forward
// icon-level-down
// icon-publish
