var m = require('mithril');

var header_phase_1 = require('views/header/header_phase_1');
var header_phase_2 = require('views/header/header_phase_2');
var header_phase_3 = require('views/header/header_phase_3');
var phaseModel = require('models/phase');

var menu_phase = function (ctrl) {

	if (phaseModel.getCurrentActive() == 1) {
		return header_phase_1(ctrl);
	} else if (phaseModel.getCurrentActive() == 2) {
		return header_phase_2(ctrl);
	} else if (phaseModel.getCurrentActive() == 3) {
		return header_phase_3(ctrl);
	}
};


module.exports = menu_phase;