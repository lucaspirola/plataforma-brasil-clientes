var m = require('mithril');
var submenu = require('views/header/loggedin_submenu');

module.exports = function (ctrl) {
	return [
		submenu(ctrl,'icon-network','Minhas Preferências','meu-perfil'),
		submenu(ctrl,'icon-log-out','Sair','sair')
	];
};
