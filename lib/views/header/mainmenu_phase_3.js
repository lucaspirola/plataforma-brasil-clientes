var m = require('mithril');

var mainmenu = function () {
    return [
            m('ul.main-menu.pure-menu-list.fix-menu', [
                m('li.pure-menu-item', [
                    m('a.menu-item[href=/comentarios-fase-1]', {config: m.route},[
                        m('i.icon-resultados'),
                        m('span.small', 'comentários'),
                        m('span.big', 'da fase 1')
                    ])
                ]),
                m('li.pure-menu-item', [
                    m('a.menu-item[href=/comentarios-fase-2]', {config: m.route},[
                        m('i.icon-discussao'),
                        m('span.small', 'comentários'),
                        m('span.big', 'da fase 2')
                    ]),
                ]),
                m('li.pure-menu-item', [
                    m('a.menu-item[href=/relatorios-fase-3]', {config: m.route},[
                        m('i.icon-relatorios'),
                        m('span.small', 'relatórios'),
                        m('span.big', 'da fase 3')
                    ]),
                ]),
            ])
    ];
};
module.exports = mainmenu;
