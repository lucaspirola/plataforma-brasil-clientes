var m = require('mithril');
var main_phase_1 = require('views/header/mainmenu_phase_1');
var main_phase_2 = require('views/header/mainmenu_phase_2');
var main_phase_3 = require('views/header/mainmenu_phase_3');
var phaseModel = require('models/phase');

var mainmenu = function (ctrl) {

	if (phaseModel.getCurrentActive() == 1) {
		return main_phase_1();
	} else if (phaseModel.getCurrentActive() == 2) {
		return main_phase_2(ctrl);
	} else if (phaseModel.getCurrentActive() == 3) {
		return main_phase_3();
	}
};


module.exports = mainmenu;