var m = require('mithril');

var submenu = function(ctrl,icon,description,url) {
    return [
        m('li.pure-menu-item',[
            m('a.pure-menu-link[href=/'+url+']', {config: m.route},[
                m('i.'+icon),
                m('span.mark-small', description)
            ])
        ])
    ];
};

module.exports = submenu;