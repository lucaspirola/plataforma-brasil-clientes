var m = require('mithril');
var submenu = require('views/header/loggedin_submenu');

module.exports = function (ctrl) {
	return [
		submenu(ctrl,'icon-traco-lapis','Minhas respostas','minhas-respostas'),
		submenu(ctrl,'icon-configuracoes','Minhas Preferências','minhas-preferencias'),
		submenu(ctrl,'icon-fechar','Sair','sair')
	];
};

