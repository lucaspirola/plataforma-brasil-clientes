var m = require('mithril');
var open_external_link = require('views/common/open_external_link');
var topbar = function () {
    return [
        m('ul.topbar-menu', [
            m('li', [
                m('a[href=/sobre]', {config: m.route}, [
                    m('i.icon-help-with-circle.pure-visible-only-sm'),
                    'sobre'
                ])
            ]),
            m('li', [
                m('a[href=http://blog.plataformabrasil.org.br/][target=_blank]', {onclick:open_external_link }, [
                    m('i.icon-discussao.pure-visible-only-sm'),
                    'blog'
                ])
            ]),
            m('li', [
                m('a[href=http://itsrio.org/civviki/][target=_blank]', {onclick:open_external_link }, [
                    m('i.icon-discussao.pure-visible-only-sm'),
                    'civviki'
                ])
            ]),
            m('li.group', [
                m('a[href=/criticas-e-sugestoes]', {config: m.route}, [
                    m('i.icon-megaphone.pure-visible-only-sm'),
                    'críticas e sugestões'
                ])
            ]),
            m('li.group', [
                m('a[href=http://facebook.com/plataformabrasil][target=_blank]', {onclick:open_external_link}, [
                    m('i.icon-megaphone.pure-visible-only-sm'),
                    'curta no facebook'
                ])
            ])
        ])
    ];
};

module.exports = topbar;
