var m = require('mithril');

module.exports = function (ctrl) {
    return [
		m('.pure-u-1-1', [
		    m('ul.all-ranking.winners-block', [
	            m('li', [
	                m('span.name.pure-u-14-24', ctrl.data),
	                m('span.bar-chart.pure-u-7-24', [
	                    m('span.bar-chart-fill', { style: { width: ctrl.score+'%' } })
	                ]),
	                m('span.percentage.pure-u-3-24', ctrl.score.toFixed(3))
	            ])
		    ])
		])
    ];
};