var m = require('mithril');
var moment = require('moment');
var allwinners_content = function(content) {
	return [
		m('.allwinners-content-item',[
		    m('.allwinners-position.pure-u-sm-2-24.pure-u-1-5', '#'+content.ranking),
	    	m('.allwinners-text.pure-u-sm-10-24.pure-u-4-5', m('',content.data)),
			m('.pure-u-1.pure-visible-only-sm', m('hr')),
			m('.pure-u-1-5.pure-visible-only-sm'),
	    	m('.allwinners-data-aux.pure-u-sm-3-24.pure-u-1-5', items_winners(content.score.toFixed(3),'pontos','icon-pontos')),
	    	m('.allwinners-data-aux.pure-u-sm-3-24.pure-u-1-5', items_winners(content.wins,'vitórias','icon-vitorias')),
	    	m('.allwinners-data-aux.pure-u-sm-3-24.pure-u-1-5', items_winners(content.losses,'perdas','icon-thumbs-down')),
	    	m('.allwinners-data-aux.pure-u-sm-3-24.pure-u-1-5', items_winners(moment(content.created_at).format('DD/MM'),'inserido em','icon-data-insercao'))
    	])
	];
};

var items_winners = function(number,text,icon) {
	return [
		m('span.allwinners-aux-number',number),
		m('br'),
		m('span.allwinners-aux-text',text),
		m('br'),
		m('i.icon-xlarger.'+icon)
	];
};

module.exports = function (ctrl) {
    return [
    	allwinners_content(ctrl)
    ];
};