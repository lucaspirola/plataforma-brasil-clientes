var m = require('mithril');
var c = require('config');
var block_title = require('views/common/block_title');
var winners = require('views/reports/winners');
var all_winners = require('views/reports/all_winners');

var regexp_mobile = /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile/i;
var isMobile = false;
if( navigator.userAgent.match(regexp_mobile) ) {
    isMobile = true;
}

var check_url = function(text_check) {
    var check_dir = "";
    if (m.route.param("orderBy") === text_check) {
        if (m.route.param("orderDirection") === "desc") {
            check_dir = "asc";
        } else {
            check_dir = "desc";
        }
    } else {
        check_dir = "desc";
    }
    return "?orderBy="+text_check+"&orderDirection="+check_dir;
};

var check_active = function(text_check) {
    var check_active = "";
    if (m.route.param("orderBy") === text_check) {
        check_active = ".active";
    } else {
        check_active = "";
    }
    return check_active;
};

module.exports = function (ctrl) {
    var view_report_winners = m.prop([]);
    var view_report_allWinners = m.prop([]);
    ctrl.winnersList().choices.slice(0,10).forEach(function(content, k) {
        view_report_winners().push(winners(content));
    });
    ctrl.allWinnersList().choices.forEach(function(content, k) {
    	var item_position = 1;
		ctrl.winnersList().choices.forEach(function(v, kk) {
			if (content.id === v.id) {
				content.ranking = item_position;
			}
        		item_position++;
		});
        view_report_allWinners().push(all_winners(content,item_position));
    });
    return [
        m('.winners-article',[
            m('h1', ctrl.page_subtitle_relatoria_fase_1()),
            m('p.justify-content', ctrl.page_intro_relatoria_fase_1()),
        ]),
        block_title(ctrl,'Temas escolhidos'),
        view_report_winners(),
        block_title(ctrl,'todos os temas'),
        m('.allwinners-title.pure-g',[
            m('.pure-u-1', m('p.allwinners-header','ordenar resultados por:')),
            m('.pure-u-1-5.pure-u-sm-2-24',[
                m('a[href="'+c.getHost()+'/?/comentarios-fase-1'+check_url('score')+'"]'+check_active('score'),[
                    m('span.icone-texto-xxlarge','#00'),
                    (isMobile ? '' : m('.hint--top.icone-ajuda.', {'data-hint':'Ranking de preferência final do tema na fase'}, m('i.icon-ajuda')) ),
                    m('','ranking')
                ])
            ]),
			m('.pure-u-sm-10-24.pure-hidden-sm'),
            m('.pure-u-1-5.pure-u-sm-3-24',[
                m('a[href="'+c.getHost()+'/?/comentarios-fase-1'+check_url('score')+'"]'+check_active('score'),[
                    m('i.icon-pontos.icon-xxlarger'),
                    (isMobile ? '' : m('.hint--top.icone-ajuda.', {'data-hint':'Pontuação de preferência calculada pelo All Our Ideas para o tema'}, m('i.icon-ajuda')) ),
                    m('','pontos')
                ])
            ]),
            m('.pure-u-1-5.pure-u-sm-3-24',[
                m('a[href="'+c.getHost()+'/?/comentarios-fase-1'+check_url('wins')+'"]'+check_active('wins'),[
                    m('i.icon-vitorias.icon-xxlarger'),
                    (isMobile ? '' : m('.hint--top.icone-ajuda.', {'data-hint':'Número de vezes em que o tema foi vencedor ao ser visualizado'}, m('i.icon-ajuda')) ),
                    m('','vitórias')
                ])
            ]),
            m('.pure-u-1-5.pure-u-sm-3-24',[
                m('a[href="'+c.getHost()+'/?/comentarios-fase-1'+check_url('losses')+'"]'+check_active('losses'),[
                    m('i.icon-thumbs-down.icon-xxlarger'),
                    (isMobile ? '' : m('.hint--left.icone-ajuda.', {'data-hint':'Número de vezes em que o tema foi perdedor ao ser visualizado'}, m('i.icon-ajuda')) ),
                    m('','perdedor')
                ])
            ]),
            m('.pure-u-1-5.pure-u-sm-3-24',[
                m('a[href="'+c.getHost()+'/?/comentarios-fase-1'+check_url('created_at')+'"]'+check_active('created_at'),[
                    m('i.icon-data-insercao.icon-xxlarger'),
                    (isMobile ? '' :  m('.hint--left.icone-ajuda.', {'data-hint':'Data em que o tema foi inserido na fase'}, m('i.icon-ajuda')) ),
                    m('','inserido em')
                ])
            ]),
        ]),
        m('.allwinners-content',[
            view_report_allWinners()
        ]),
    ];
};