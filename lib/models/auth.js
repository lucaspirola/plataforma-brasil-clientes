/*global localStorage: false, console: false, $: false */
var c = require('config');
var m = require('mithril');
var headerM = require('controllers/header');

/*istanbul ignore next*/
var Auth = module.exports = {
    token: m.prop(localStorage.token),

    // trade credentials for a token
    login: function(email, password) {
        return m.request({
                method: 'POST',
                url: c.getApiHost() + '/login',
                data: {
                    email: email,
                    password: password
                },
                unwrapSuccess: function(res) {
                    ga('set', '&uid', email);
                    localStorage.token = res.token;
                    return res.token;
                }
            })
            .then(this.token);
    },

    // forget token
    logout: function() {
        this.token(false);
        delete localStorage.token;
    },

    // signup on the server for new login credentials
    // register: function(email, password){
    //   return m.request({
    //     method: 'POST',
    //     url: '/users',
    //     data: {email:email, password:password}
    //   });
    // },

    // // ensure verify token is correct
    // verify: function(token){
    //   return m.request({
    //     method: 'POST',
    //     url: '/users/verify',
    //     data: {token: token}
    //   });
    // },

    // make an authenticated request
    req: function(options) {
        if (typeof options == 'string') {
            options = {
                method: 'GET',
                url: options
            };
        }
        var oldConfig = options.config || function() {};
        var oldExtract = options.extract || function() {};
        options.extract = function(xhr, xhrOptions) {
            if (xhr.status == 401) {
                localStorage.clear();
                m.module(document.getElementById('header'), headerM);
                m.route('/entrar?back_to='+m.route());
            }
            oldExtract(xhr, xhrOptions);
            if (xhr.responseText === null) {
                return {};
            }
            return xhr.responseText;
        };
        options.config = function(xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + Auth.token());
            oldConfig(xhr);
        };

        // try request, if auth error, redirect
        // TODO: remember where the user was, originally
        var deferred = m.deferred();
        m.request(options).then(deferred.resolve, deferred.resolve);

        return deferred.promise;
    }
};
