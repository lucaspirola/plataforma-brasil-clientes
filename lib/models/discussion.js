var m = require('mithril');
var c = require('config');
var authModel = require('models/auth');
var userModel = require('models/user');

var discussion = {

	total_records: m.prop(0),
	INTIAL_RANGE: '0-10',

	get: function (id) {
		if (userModel.isAuthenticate('')) {
			return authModel.req({method:'GET',url: c.getApiHost() + '/discussions/' + id});
		} else {
			return m.request({method:'GET',url: c.getApiHost() + '/discussions/' + id});
		}
	},
	get_all: function (theme_id, order, category, author) {
		var xhrConfig = function(xhr) {
		    xhr.setRequestHeader('Content-Range', discussion.INTIAL_RANGE);
		};
		var extract = function(xhr, xhrOptions) {
			var total_records = 0;
			var range = xhr.getResponseHeader("Content-Range").match(/(\d+)$/);

			if (range) {
				total_records = range[0];
		    }
		    discussion.total_records(total_records);
		};
		var url = c.getApiHost() + '/discussions?theme_id=' + theme_id;

		if ( category !== undefined ) {
			url = url+'&category='+category;
		}

		if ( (order !== undefined) && (order === 'mais-novas') ) {
			url = url+'&order=recent';
		} else if ( (order !== undefined) && (order === 'mais-antigas') ) {
			url = url+'&order=oldest';
		} else if ( (order !== undefined) && (order === 'mais-frias') ) {
			url = url+'&order=coldest';
		}

		if ( ( author !== undefined ) && (author !== '') ) {
			url = url+'&author='+author;
		}

		if (userModel.isAuthenticate('')) {
			return authModel.req({method:'GET',url: url, config: xhrConfig, extract: extract });
		} else {
			return m.request({method:'GET',url: url, config: xhrConfig, extract: extract});
		}
	},
	like: function (discussion_id) {
		return authModel.req({
			method: 'GET',
			url: c.getApiHost() + '/discussions/' + discussion_id + '/like'
		});
	},
	dislike: function (discussion_id) {
		return authModel.req({
			method: 'GET',
			url: c.getApiHost() + '/discussions/' + discussion_id + '/dislike'
		});
	},

	save_discussion : function (ctrl, e) {
		// Initialize a new validator
		var validator = new m.validator({
			// Check model name property
			description: function (desc) {
				if (!desc) {
					return "O campo opnião não pode ficar em branco.";
				}
			},
			categories: function(cat) {
				if (cat.length < 2) {
					return "É preciso indicar ao menos 2 categorias.";
				}
			}
		});
		validator.validate(ctrl.model);
		var msg = ctrl.model.message([]);
		if (validator.hasError('description')) {
			msg.push({type:'error', text: validator.hasError('description')});
		}
		if (validator.hasError('categories')) {
			msg.push({type:'error', text: validator.hasError('categories')});
		}
		if (msg.length === 0) {
			authModel.req({
				method: 'POST', 
				url: c.getApiHost() + '/discussions', 
				data: ctrl.model
			}).then(function (data) {
				ctrl.model.title('');
				ctrl.model.description('');
				ctrl.model.categories([]);
				msg.push({type:'success', text: 'Comentário enviado com sucesso.'});
				ctrl.list_discussions().unshift(data);
			});
		}
		return false;
	},

	get_more_discussions: function (ctrl, range){
		var xhrConfig = function(xhr) {
		    xhr.setRequestHeader('Content-Range', range);
		};
		var category = ctrl.category();
		var order = ctrl.order();
		var url = c.getApiHost() + '/discussions?theme_id=' + ctrl.theme_active().id;

		if ( category !== undefined ) {
			url = url+'&category='+category;
		}

		if ( (order !== undefined) && (order === 'mais-novas') ) {
			url = url+'&order=recent';
		}
		if (userModel.isAuthenticate('')) {
			authModel.req({method:'GET',url: url, config: xhrConfig }).
				then(ctrl.updateListAfterRequest);
		} else {
			m.request({method:'GET',url: url, config: xhrConfig}).
				then(ctrl.updateListAfterRequest);
		}

		return false;
	},

	get_mine: function (user_id) {
		return authModel.req({method:'GET', url: c.getApiHost() + '/users/'+user_id+'/discussions'});
	},

	flag: function (discussion_id) {
		return authModel.req({method:'POST', url: c.getApiHost() + '/discussions/' + discussion_id + '/flag'});
	},

    notification_read: function (topic_discussion_id) {
		return authModel.req({method:'GET', url: c.getApiHost() + '/notifications/discussions/'+topic_discussion_id});
    }
};

module.exports = discussion;
