var m = require('mithril');
var c = require('config');
require('mithril-validator')(m);
var authModel = require('models/auth');
var userModel = require('models/user');

var comment = {

	total_records: m.prop(0),
	INTIAL_RANGE: '0-30',

	get_all_by_discussion: function (discussion_id) {
		var xhrConfig = function(xhr) {
		    xhr.setRequestHeader('Content-Range', comment.INTIAL_RANGE);
		};
		var extract = function(xhr, xhrOptions) {
			var total_records = 0;
			var range = xhr.getResponseHeader("Content-Range").match(/\d\d$/);
			
			if (range) {
				total_records = range[0];
		    }
		    comment.total_records(total_records);
		};
		if (userModel.isAuthenticate('')) {
			return authModel.req({ method: 'GET', url: c.getApiHost() + '/discussions/' + discussion_id + '/comments', config: xhrConfig, extract: extract});
		} else {
			return m.request({ method: 'GET', url: c.getApiHost() + '/discussions/' + discussion_id + '/comments', config: xhrConfig, extract: extract});
		}
	},

	like: function (discussion_id, comment_id) {
		return authModel.req({
			method: 'GET',
			url: c.getApiHost() + '/discussions/' + discussion_id + '/comments/' + comment_id + '/like'
		});
	},
	dislike: function (discussion_id, comment_id) {
		return authModel.req({
			method: 'GET',
			url: c.getApiHost() + '/discussions/' + discussion_id + '/comments/' + comment_id + '/dislike'
		});
	},

	save_comment : function (ctrl, e) {
		// Initialize a new validator
		var validator = new m.validator({
			// Check model name property
			description: function (desc) {
				if (!desc) {
					return "O comentário não pode ser vazio.";
				}
			}
		});
		validator.validate(ctrl.model);

		if (validator.hasErrors()) {
			var msg = ctrl.model.message([]);
			msg.push({type:'error', text: validator.hasError('description')});
		} else {
			authModel.req({
				method: 'POST', 
				url: c.getApiHost() + '/discussions/'+ctrl.discussion_id()+'/comments', 
				data: ctrl.model
			}).then(function (data) {
				ctrl.model.description('');
				var msg = ctrl.model.message([]);
				msg.push({type:'success', text: 'Comentário enviado com sucesso.'});
				ctrl.comments().unshift(data);
			});
		}

		return false;
	},

	get_more_comments: function (ctrl, range, e){

		var xhrConfig = function(xhr) {
		    xhr.setRequestHeader('Content-Range', range);
		};

		if (userModel.isAuthenticate('')) {
			authModel.req({ 
				method: 'GET', 
				url: c.getApiHost() + '/discussions/' + ctrl.discussion_id() + '/comments',
				config: xhrConfig
			}).then(ctrl.updateListAfterRequest);
		} else {
			return m.request({
				method: 'GET', 
				url: c.getApiHost() + '/discussions/' + ctrl.discussion_id() + '/comments', 
				config: xhrConfig
			}).then(ctrl.updateListAfterRequest);
		}

		return false;
	},

	get_mine: function () {
		// return m.request({url: c.getApiHost() + '/my_discussions')
	},

	flag: function (discussion_id, comment_id) {
		return authModel.req({method:'POST', url: c.getApiHost() + '/discussions/' + discussion_id + '/comments/' + comment_id + '/flag'});
	},

    notification_read: function (topic_comment_id) {
		return authModel.req({method:'GET', url: c.getApiHost() + '/notifications/discussion_comments/'+topic_comment_id});
    }
};

module.exports = comment;
