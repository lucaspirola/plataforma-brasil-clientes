var m = require('mithril');
var c = require('config');
var phaseModel = require('models/phase');
var theme = {
	get: function (id) {
		return m.request({ method: 'GET', url: c.getApiHost() + '/themes/' + id });
	},

	getAll : function () {
		return m.request({ method: 'GET', url: c.getApiHost() + '/themes' });
	},
};

module.exports = theme;
