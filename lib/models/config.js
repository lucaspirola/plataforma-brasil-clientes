var m = require('mithril');
var c = require('config');
var authModel = require('models/auth');

var configuration = {
	users : m.prop([]),
    sites : m.prop([]),

    get : function (key, data) {
		var config = {};
        data.map(function(v,k) {
			if (v.key === key) {
				config = v.value;
			}
		});

        return config;
    },

    get_all_site : function () {
		return m.request({method: 'get', url: c.getApiHost() + '/configs'});
    },

	get_all_user : function () {
		return authModel.req({method: 'get', url: c.getApiHost() + '/users/configs'});
	},
    
    get_site : function (key) {
        var that = this;
        return this.get_all_site().then(function(data) {
            return that.get(key, data);
        });
    },

	get_user: function (key) {
        var that = this;
        return this.get_all_user().then(function(data) {
            return that.get(key, data);
        });
	},

	save_user: function (key, value) {
		return authModel.req({method: 'POST', url: c.getApiHost() + '/users/configs', data: {'key':key, 'value': value}});
	}
};

module.exports = configuration;
