var m = require('mithril');
var c = require('config');
var Auth = require('models/auth');
var headerM = require('controllers/header');
var facebookCode = m.prop('');

function saveLogin(data) {
    var deferred = m.deferred();
    localStorage.token = data.token;
    // setup header login
    isAuthenticate(data.token)
        .then(function(u) {
            localStorage.user = JSON.stringify(u);
            deferred.resolve(u);
        });
    return deferred.promise;
}

/**
 * This method is customized to better handle userDetails
 */

function isAuthenticate(t) {
    var deferred = m.deferred();

    if (("user" in localStorage) && (localStorage.user !== "")) {
        var data = JSON.parse(localStorage.user);
        deferred.resolve(data);
        return deferred.promise;
    }

    if (t !== "") {
        Auth.token(t);
    } else {
        deferred.resolve({});
    }

    if ((Auth.token() !== "") && (Auth.token() !== undefined)) {
        getUser().then(function(data) {
            deferred.resolve(data);
        });
    }

    return deferred.promise;
}

function setFacebookCode() {
    var access_token = m.route.param("access_token");
    if ((access_token !== "") && (access_token !== "contaporemail")) {
        facebookCode(m.route.param("access_token"));
        return true;
    }
    return false;
}

function getFacebookData() {
    return m.request({
        method: 'GET',
        url: c.getApiHost() + '/facebook?code=' + facebookCode(),
    });
}

function getUser() {
    return Auth.req(c.getApiHost() + '/users');
}

function editUser(user) {
    return Auth.req({
        method: 'PUT',
        url: c.getApiHost() + '/users',
        data: user
    });
}
function saveUser(user) {
    return m.request({
        method: 'POST',
        url: c.getApiHost() + '/new_user',
        data: user
    });
}

function get_by_id (id) {
	return Auth.req({
		method: 'GET',
		url: c.getApiHost() + '/profile/' + id
	});
}

function get_discussions_notifications(user) {
	return Auth.req({
		method: 'GET',
		url: c.getApiHost() + '/notifications/discussions'
	});
}
function get_comments_notifications(user) {
	return Auth.req({
		method: 'GET',
		url: c.getApiHost() + '/notifications/discussion_comments'
	});
}
function get_notifications(user) {
	return m.sync([
		get_discussions_notifications(user),
		get_comments_notifications(user)
	]);
}
function get_total_notifications(user) {
	return Auth.req({
		method: 'GET',
		url: c.getApiHost() + '/notifications/unreaded'
	});
}
module.exports = {
    setFacebookCode: setFacebookCode,
    getFacebookData: getFacebookData,
    facebookCode: facebookCode,
    saveUser: saveUser,
    getUser: getUser,
    isAuthenticate: isAuthenticate,
    saveLogin: saveLogin,
    editUser: editUser,
	get_by_id: get_by_id,
	get_notifications : get_notifications,
    get_total_notifications: get_total_notifications
};
