var m = require('mithril');
var c = require('config');
var feature_box = require('views/common/feature_box');
var userModel = require('models/user');
var Auth = require('models/auth');
var _ = require('underscore');
var creditos_foto = require('views/common/creditos_foto');
var results = {};

results.controller = function() {
    results.vm.init();
    this.vm = results.vm;

    this.ativarFiltro = function(ctrl, e) {
        e.preventDefault();
        var id = e.target.id.split("-");
        var type = id[0];
        var val = id[1];
        var filter_url = '';

        if (type == 'sector') {
            filter_url = c.getApiHost() + '/priorities/filter/'+val+'/all';
        } else {
            filter_url = c.getApiHost() + '/priorities/filter/all/'+val;
        }

        [].forEach.call(document.getElementsByClassName('region'), function(e) {
            e.childNodes[0].classList.remove('active');
        });
        [].forEach.call(document.getElementsByClassName('sector'), function(e) {
            e.childNodes[0].classList.remove('active');
        });
        setTimeout(function() {
            document.getElementById(type+'-'+val).classList.add('active');
        }, 500);

        Auth.req({
            method: 'GET',
            url: filter_url
        }).then(function(data) {
            results.vm.filtred_results = data;
        });
    };
};

results.vm = {
    init: function() {
        results.vm.options = m.prop({});
        results.vm.filtred_results = ["selecione"];

        var u = userModel.isAuthenticate();
        if ((u() !== undefined) && (u() !== "")) {

            // API do allourideas não suporta ponto na identificação, removendo...
            var visitor_identifier = u().email.replace(/@/g, '');
            visitor_identifier = visitor_identifier.replace(/\./g, '');

            Auth.req({
                method: 'GET',
                url: c.getApiHost() + '/priorities/results'
            }).then(function(data) {
                var r = [];
                // padronizando resposta da API
                _(data.choices.choice).each(function(v, k) {
                    r.push({
                        name: v.data,
                        value: v.score['#text']
                    });
                });
                results.vm.options.results = r;
            }, function(err) {
                console.log(err);
            });
        } else {
            m.route('/entrar');
        }
    }
};


results.view = function(ctrl) {
    return m('#results', [
        m('.hero.inner',
            m('.container', [
                m('.pure-g', feature_box(
                    '',
                    [m('h2', 'priorização de temáticas: de maio a junho'),
                    m('h3', 'RESULTADO GERAL')]
                ))
            ]),
            creditos_foto()
        ),
        m('.more-results-details', [
            m('.container', [
                m('.pure-g', [
                    m('.pure-u-1.pure-u-sm-12-24',
                        m('h1', "REFORMA POLÍTICA"),
                        m('h2', 'resultado parcial fase 01')
                    ),
                    m('.pure-u-1.pure-u-sm-12-24', [
                        m('p', 'Este é o resultado geral da priorização até o momento. É possível visualizar os 6 temas com melhor pontuação, em uma escala de 0 a 100 pontos. A pontuação representa a chance estimada que determinado tema será escolhido em relação a outro. Por exemplo, uma pontuação 100 significa que é esperado que determinado tema será escolhido todas as vezes em relação aos outros. Uma pontuação zero significa que é esperado que determinado tema não será escolhido nenhuma vez em relação aos outros.')
                    ]),
                    m('.pure-u-1-1', [
                        m('ul.all-ranking', [
                            ctrl.vm.options.results.map(function(r, i) {
                                return m('li', [
                                    m('span.name.pure-u-12-24', r.name),
                                    m('span.bar-chart.pure-u-9-24', [
                                        m('span.bar-chart-fill', {
                                            style: {
                                                width: Math.round(r.value) + '%'
                                            }
                                        })
                                    ]),
                                    m('span.percentage.pure-u-3-24', Math.round(r.value))
                                ]);
                            })
                        ])
                    ]),
                    m('.pure-u-1', [
                        m('a.pure-button.big[href=/fase-1-meus-resultados]', {config: m.route},'meus resultados')
                    ])
                ])
            ])
        ])
    ]);
};

module.exports = results;
