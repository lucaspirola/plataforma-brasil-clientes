var m = require('mithril');
var userModel = require('models/user');
var Velocity = require('velocity-animate');
var header = require('views/header');
var themeModel = require('models/theme');
var phaseModel = require('models/phase');
header.controller = function() {
    this.vm = header.vm;
    this.vm.init(this);
    var ctrl = this;
    ctrl.current_user = m.prop(userModel.isAuthenticate(localStorage.user));

    userModel.get_total_notifications(ctrl.current_user).then(function (total) {
        ctrl.vm.total_notifications(total.total);
    });
    this.toggleMenu = function (e) {
        e.preventDefault();
        if (this.className === '') {
            this.className = 'open';
            Velocity(document.getElementById("mobile-menu"), {left: 0});
        } else {
            this.className = '';
            Velocity(document.getElementById("mobile-menu"), {left: '-35em'});
        }
    };
};

header.vm = {};
header.vm.init = function(ctrl) {
    ctrl.vm.user = m.prop({});
    ctrl.vm.total_notifications = m.prop(0);
    userModel.isAuthenticate("")
        .then(function(u) {
            ctrl.vm.user(u);
        });
    if (phaseModel.getCurrentActive() == 2) {
        ctrl.themes = m.prop();
        themeModel.getAll().then(function(data) {
            ctrl.themes(data);
        });
    }
};

module.exports = header;
