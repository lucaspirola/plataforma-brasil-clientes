var m = require('mithril');
var view = require('views/handyman');
var discussionModel = require('models/discussion');
var handymanModel = require('models/handyman');
var handyman = {};

handyman.controller = function () {
	var ctrl = this;
	var discussion_id = m.route.param("id");
	ctrl.featured_discussion = m.prop(discussionModel.get(discussion_id));
	ctrl.list_handyman = m.prop(handymanModel.getAll(discussion_id));
};

handyman.view = view;

module.exports = handyman;
