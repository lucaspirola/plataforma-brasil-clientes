var m = require('mithril');
var c = require('config');
var Auth = require('models/auth');
var userModel = require('models/user');
var Velocity = require('velocity-animate');
var tips = require('views/vote/tips');
var initial_box = require('views/vote/initial_box');
var creditos_foto = require('views/common/creditos_foto');
var invite_friend = require('views/vote/invite_friend');
var Vote = module.exports = {
    controller: function() {
        var ctrl = this;
        ctrl.options = m.prop({});
        ctrl.abertoNovaOpcao = m.prop(false);
        ctrl.abertoAjudaAmigo = m.prop(false);
        ctrl.statusNovaOpcao = m.prop('');
        ctrl.contentNovaOpcao = m.prop('');
        ctrl.emailParaAmigo = m.prop('');
        ctrl.mensagemParaAmigo = m.prop("Olá, \n\nEstou na Plataforma Brasil escolhendo os principais desafios da Reforma Política. Acho importante sua participação neste processo! \n\nAproveite para visitar a Plataforma e priorizar também!");
        ctrl.statusEnviarAjuda = m.prop('');

        m.request({
            method: 'GET',
            url: c.getApiHost() + '/choices'
        }).then(ctrl.options);

        ctrl.pularOpcoes = function(ctrl, e) {
            e.preventDefault();

            var u = userModel.isAuthenticate();
            if ((u() !== undefined) && (u() !== "") && false) {
                var visitor_identifier = u().email.replace(/@/g, '');
                visitor_identifier = visitor_identifier.replace(/\./g, '');
                Auth.req({
                    method: 'POST',
                    url: c.getApiHost() + '/priorities/skip?prompt_id=' + this.options()['prompt-id'],
                    data: {
                        vote: {
                            visitor_identifier: visitor_identifier
                        },
                        next_prompt: {
                            visitor_identifier: visitor_identifier
                        }
                    }
                }).then(this.options, function(err) {
                    console.log(err);
                });
            } else {
                m.route('/entrar');
            }
        };

        ctrl.abrirAjudaAmigo = function(ctrl, e) {
            e.preventDefault();

            this.abertoAjudaAmigo(true);
        };

        ctrl.fecharAjudaAmigo = function(ctrl, e) {
            e.preventDefault();

            this.abertoAjudaAmigo(false);
        };

        ctrl.enviarAjudaAmigo = function(ctrl, e) {
            e.preventDefault();

            if ((this.emailParaAmigo() === "") || (this.mensagemParaAmigo() === "")) {
                this.statusEnviarAjuda(m(".alert.alert-error", {
                    config: this.fadesIn
                }, "Obrigatório informar o email e a mensagem que você quer enviar."));
                return;
            } else {
                Auth.req({
                    method: 'POST',
                    url: c.getApiHost() + '/users/invite',
                    data: {
                        email: this.emailParaAmigo(),
                        message: this.mensagemParaAmigo().replace(/(?:\r\n|\r|\n)/g, '<br />') + '<br><a href="'+c.getHost()+'/fase-1-votar?invite">Acesse clicando aqui.</a>'
                    }
                });
                this.emailParaAmigo("");
                this.statusEnviarAjuda(m(".alert.alert-success", {
                    config: this.fadesIn
                }, "Convite enviado com sucesso."));
            }
        };

        ctrl.fecharNovaOpcao = function(ctrl, e) {
            e.preventDefault();

            this.abertoNovaOpcao(false);
        };

        ctrl.abrirNovaOpcao = function(ctrl, e) {
            e.preventDefault();

            this.abertoNovaOpcao(true);
        };

        this.fadesIn = function(element, isInitialized, context) {
            if (!isInitialized) {
                element.style.opacity = 0;
                Velocity(element, {
                    opacity: 1
                });
            }
        };

        ctrl.salvarNovaOpcao = function(ctrl, e) {
            e.preventDefault();

            if (this.contentNovaOpcao() === "") {
                this.statusNovaOpcao(m(".alert.alert-error", {
                    config: this.fadesIn
                }, "Preencha com uma opção válida o campo acima."));
                return;
            } else {

                var u = userModel.isAuthenticate();
                if ((u() !== undefined) && (u() !== "")) {
                    var visitor_identifier = u().email.replace(/@/g, '');
                    visitor_identifier = visitor_identifier.replace(/\./g, '');

                    Auth.req({
                        method: 'POST',
                        url: c.getApiHost() + '/priorities/choices',
                        data: {
                            "visitor_identifier": visitor_identifier,
                            "data": this.contentNovaOpcao()
                        }
                    });

                    this.contentNovaOpcao("");
                    this.statusNovaOpcao(m(".alert.alert-success", {
                        config: this.fadesIn
                    }, "Opção incluída com sucesso e aguarda moderação."));
                } else {
                    m.route('/entrar');
                }
            }
        };

        ctrl.sendVote = function(id) {
            var direction, choice_text, choice_id;

            if (ctrl.options()['left-choice-id'] === id) {
                direction = 'left';
                choice_text = ctrl.options()['left-choice-text'];
                choice_id = ctrl.options()['left-choice-id'];
            } else {
                direction = 'right';
                choice_text = ctrl.options()['right-choice-text'];
                choice_id = ctrl.options()['right-choice-id'];
            }

            var u = userModel.isAuthenticate();
            if ((u() !== undefined) && (u() !== "") && false) {
                var visitor_identifier = u().email.replace(/@/g, '');
                visitor_identifier = visitor_identifier.replace(/\./g, '');

                Auth.req({
                    method: 'POST',
                    url: c.getApiHost() + '/priorities/vote?prompt_id=' + ctrl.options()['prompt-id'],
                    data: {
                        vote: {
                            visitor_identifier: visitor_identifier,
                            direction: direction,
                            choice_text: choice_text,
                            choice_id: choice_id
                        },
                        next_prompt: {
                            visitor_identifier: visitor_identifier
                        }
                    }
                }).then(ctrl.options, function(err) {
                    console.log(err);
                });
            } else {
                m.route('/entrar');
            }
        };
    },

    view: function(ctrl) {
        return [
        tips(),
        m('.box-vote.hero',[
            m('.container', [
                m('.pure-u-sm-4-24.pure-hidden-sm'),
                m('.featured-box.pure-u-1.pure-u-sm-16-24',[
                    m('h1', 'Qual dentre essas duas opções você acha mais importante para uma Reforma Política do século 21?')
                ]),
                ctrl.abertoAjudaAmigo() ? '' : initial_box(ctrl),
                ctrl.abertoAjudaAmigo() ? invite_friend(ctrl) : '',
                m('.pure-g', [
                    m('.pure-u-sm-4-24.pure-hidden-sm'),
                    m('.pure-u-1.pure-u-sm-16-24.results-cta', [
                        m('p', [
                            'a qualquer momento, ',
                            m('a[href=/fase-1-meus-resultados]', {
                                config: m.route
                            }, "veja seus resultados"),
                            '.'
                        ])
                    ])
                ]),
                creditos_foto()
            ])
        ])
    ];
    }
};
