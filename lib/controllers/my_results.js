var m = require('mithril');
var feature_box = require('views/common/feature_box');
var userModel = require('models/user');
var Auth = require('models/auth');
var creditos_foto = require('views/common/creditos_foto');
var phaseModel = require('models/phase');
var share_fb_dialog = require('views/common/share_fb_dialog');
var c = require('config');
var my_results = {};

my_results.controller = function() {
    var ctrl = this;
    ctrl.vm = my_results.vm;
    ctrl.vm.init();
    ctrl.c = c;

    ctrl.compartilhar = function(ctrl, e) {
		share_fb_dialog(
					'Eu priorizei: "'+my_results.vm.options.results[0].name+'". E você, já participou?',
					' EU RESPONDI '+my_results.vm.options.summary.votes+' VEZES.'+
                    ' PULEI '+my_results.vm.options.summary.skips+' OPÇÕES.'+
                    ' SUGERI '+my_results.vm.options.summary.suggestions+' NOVAS OPÇÕES.'+
                    ' E CONVIDEI '+my_results.vm.options.summary.invites+' AMIGOS',
					'/fase-1-votar'
	   );
        return false;
	};
};
my_results.vm = {

	phase_1_is_active : function () {
		if (phaseModel.getCurrentActive() == 1) {
			return true;
		}
		return false;
	},

    init: function() {
        my_results.vm.options = m.prop({});

        var u = userModel.isAuthenticate();
        if ((u() !== undefined) && (u() !== "")) {
            var visitor_identifier = u().email.replace(/@/g, '');
            visitor_identifier = visitor_identifier.replace(/\./g, '');

            Auth.req({
                method: 'GET',
                url: c.getApiHost() + '/priorities/my_results'
            }).then(function(data) {
                var results = [];
                var total_votes = 0;

                for (var key in data) {
                    total_votes = total_votes + data[key];
                    results.push({
                        name: key,
                        value: data[key]
                    });
                }

                results = results.map(function(o) {
                    o.percentage = o.value / total_votes * 100;
                    return o;
                });

                results.sort(function(a, b) {
                    return b.value - a.value;
                });
                my_results.vm.options.results = results;
            }, function(err) {
                console.log(err);
            });


            Auth.req({
                method: 'GET',
                url: c.getApiHost() + '/priorities/summary'
            }).then(function(data) {
                my_results.vm.options.summary = data;
            });
        } else {
            m.route('/entrar');
        }
    }
};


my_results.view = function(ctrl) {
    return m('#results', [
        m('.hero.inner',
            m('.container', [
                m('.pure-g' , feature_box(
                    '',
                    [m('h1', 'MEUS RESULTADOS'),
                    m('h2', 'veja quais foram os temas que você priorizou')]
                )),
                creditos_foto()
            ])
        ),
        m('.more-results-details', [
            m('.container', [
                m('.pure-g', [
                    m('.pure-u-1.pure-u-sm-12-24.box-summary', [
                        m('h1', 'VOCÊ PRIORIZOU "'+ctrl.vm.options.results[0].name+'"'),
                        m('ul', [
                            m('li', m('h3', '>> DEU '+ctrl.vm.options.summary.votes+' RESPOSTAS')),
                            m('li', m('h3', '>> PULOU '+ctrl.vm.options.summary.skips+' OPÇÕES')),
                            m('li', m('h3', '>> ENVIOU '+ctrl.vm.options.summary.suggestions+' SUGESTÕES')),
                            m('li', m('h3', '>> CONVIDOU '+ctrl.vm.options.summary.invites+' AMIGOS')),
                        ]),
                        m('a.pure-button.fb.big[href=#]', {
                            onclick: ctrl.compartilhar.bind(ctrl, this)
                        }, [
                            m('i.icon-facebook'),
                            'Compartilhar'
                        ]),
                        m('br'),
                        (ctrl.vm.phase_1_is_active() ? m('a.pure-button.big[href=/fase-1-votar]', {config: m.route}, [
                            m('i.icon-relogio'),
                            'priorizar novamente'
                        ]) : ''),
                        m('a.pure-button.big[href=/fase-1-resultados]', {config: m.route}, [
                            m('i.icon-resultados'),
                            'ver resultados gerais'
                        ])
                    ]),
                    m('.pure-u-1.pure-u-sm-12-24.box-ranking', [
                        m('ul.my-ranking', [
                            ctrl.vm.options.results.map(function(r, i) {
                                return m('li', [
                                    m('span.name', r.name),
                                    m('span.percentage', Math.round(r.percentage) + '%')
                                ]);
                            })
                        ])
                    ]),
                ])
            ])
        ])
    ]);
};

module.exports = my_results;
