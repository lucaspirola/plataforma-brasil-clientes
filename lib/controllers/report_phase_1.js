var m = require('mithril');
var header_report = require('views/common/header_report');
var box_report_phase_1 = require('views/reports/report_phase_1');
var getWinners = require('models/reports');
var configModel = require('models/config');

//var md2html = require('markdown2mithril');

function controller () {
    // this.winnersList = m.prop(getWinners.getWinners());

    var ctrl = this;
    var r = [];

    var order = m.route.param("orderBy");
    var direction = m.route.param("orderDirection");

    this.winnersList = m.prop(getWinners.getWinners());
    this.allWinnersList = m.prop(getWinners.getReports_phase1(order, direction));

    ctrl.page_subtitle_relatoria_fase_1 = m.prop();
	ctrl.page_title_relatoria_fase_1 = m.prop();
    ctrl.page_intro_relatoria_fase_1 = m.prop();

    configModel.get_site('page_subtitle_relatoria_fase_1').then(function(data) { ctrl.page_subtitle_relatoria_fase_1(data); });
    configModel.get_site('page_title_relatoria_fase_1').then(function(data) { ctrl.page_title_relatoria_fase_1(data); });
    configModel.get_site('page_intro_relatoria_fase_1').then(function(data) { ctrl.page_intro_relatoria_fase_1(data); });
}

function view(ctrl) {
    return m('.page-report.page-report-1', [
        header_report(ctrl, ctrl.page_title_relatoria_fase_1(),'img/illustration/step1.svg'),
        m('.container', [
            box_report_phase_1(ctrl)
        ])
    ]);
}

module.exports = {
    controller: controller,
    view: view
};