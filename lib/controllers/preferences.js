var m = require('mithril');
var userModel = require('models/user');
var configModel = require('models/config');
var Auth = require('models/auth');
var headerM = require('controllers/header');
var _ = require('underscore');
var c = require('config');
var main_form = require('views/signup/form');

var EditAccount = {};

EditAccount.user = m.prop();

EditAccount.controller = function() {
	this.user = EditAccount.user(JSON.parse(localStorage.user));
	this.caracteresRestantes = m.prop("0");
	var ctrl = this;
	ctrl.error = m.prop('');

	this.isNewRecord = false;
	this.vm = EditAccount.vm;
	this.vm.nome(this.user.nome);
	this.vm.photo_url(this.user.photo_url);
	this.vm.facebook_id(this.user.facebook_id);
	this.vm.email(this.user.email);
	this.vm.genero(this.user.genero);
	this.vm.sector(this.user.sector);
	this.vm.data_nascimento(this.user.data_nascimento);
	this.vm.ocupacao(this.user.ocupacao);
	this.vm.cidade(this.user.cidade);
	this.vm.estado(this.user.estado);
	this.vm.cpf(this.user.cpf);
	this.vm.descricao(this.user.descricao);

	if (this.user.facebook_id !== "") {
		this.vm.facebookSignup = true;
	}
	m.request({
		method: 'GET',
		url: './json/states.json'
	}).then(function(data) {
		data.unshift({
			id: '',
			name: 'Estado'
		});
		EditAccount.vm.states(data);
		EditAccount.vm.changeState(EditAccount.vm.estado());
		EditAccount.vm.statesDisabled = false;
	});


	this.vm.facebookSignup = true;
	if (this.user.facebook_id === "") {
		this.vm.facebookSignup = false;
	}
	this.vm.popupTagOpened = true;

	this.trocarFoto = function (ctrl) {
		//var ctrl = this;
		//e.preventDefault();
		var deferred = m.deferred();
		var pic = ctrl.vm.profilePicture();
		if (pic && pic[0]) {
			var reader = new FileReader();
			reader.onload = function (ee) {
				ctrl.vm.photo_url(ee.target.result);
				m.redraw(true); // force

				var formData = new FormData();
				formData.append("data", pic[0]);

				Auth.req({
					method: "POST",
					url: c.getApiHost() + "/users/avatar",
					data: formData,
					serialize: function(value) { return value; }
				}).then(function (image_uploaded) {
					var u = JSON.parse(localStorage.user);
					u.photo_url = image_uploaded.photo_url;
					localStorage.user = JSON.stringify(u);
					deferred.resolve(image_uploaded);
				});
			};
			reader.readAsDataURL(pic[0]);
		}
		return deferred.promise;
	};

	this.contaCaracteres = function(value, e) {
		ctrl.vm.descricao(e.target.value);
		ctrl.caracteresRestantes(500 - parseInt(_(e.target.value).size()));
	};

	this.save = function(e) {
		e.preventDefault();
		var ctrl = this;

		var u = {
			email: ctrl.vm.email(),
			cpf: ctrl.vm.cpf(),
			nome: ctrl.vm.nome(),
			photo_url: ctrl.vm.photo_url(),
			genero: ctrl.vm.genero(),
			data_nascimento: ctrl.vm.data_nascimento(),
			ocupacao: ctrl.vm.ocupacao(),
			cidade: ctrl.vm.selectedCitie(),
			estado: ctrl.vm.selectedState(),
			descricao: ctrl.vm.descricao(),
			facebook_id: ctrl.vm.facebook_id()
		};

		userModel.editUser(u)
			.then(function(data) {
					userModel.saveLogin(localStorage)
						.then(function () { return ctrl.trocarFoto(ctrl); })
						.then(function () {
							m.module(document.getElementById('header'), headerM);
							localStorage.pre_sign_up = '';
						});
					var message = 'Sucesso na alteração do perfil. Suas informações estarão atualizadas no próximo login.';
					ctrl.error(m('.alert-success', message));
			}, function(err) {
				ctrl.error(m(".alert-error", err.message));
			});
	};

	this.save_config = function (ctrl, e) {
		var name = e.currentTarget.name;
		var value = e.currentTarget.value;

		configModel.save_user(name, value).then(function (data) {
			//console.log(data);
		});

		return false;
	};

	ctrl.isPrivate = m.prop(false);
    
    if (configModel.get_user('privacy') === 'private') {
        ctrl.isPrivate(true);
    }
};
EditAccount.vm = {
	nome: m.prop(""),
	photo_url: m.prop("img/unknown.jpg"),
	facebook_id: m.prop(""),
	email: m.prop(""),
	cidade: m.prop(""),
	estado: m.prop(""),
	descricao: m.prop(""),
	ocupacao: m.prop(""),
	data_nascimento: m.prop(""),
	genero: m.prop(""),
	sector: m.prop(""),
	cpf: m.prop(""),
	password: m.prop(""),
	sectorPopup: m.prop("br"),
	sectorDescriptionPopup: m.prop("Quero colaborar no site como cidadão e não me sinto parte de nenhum dos outros setores listados."),

	profilePicture: m.prop(""),

	fecharTagPopup: function() {
		EditAccount.vm.popupTagOpened = false;
	},

	openPopupClick: function(ctrl, e) {
		var msg = {
			'gov': 'Meu trabalho é em algum ramo do Estado, seja ele o executivo, judiciário ou legislativo, em qualquer esfera da federação (União, Estados e Municípios).',
			'edu': 'Sou professor, pesquisador ou alguém que trabalha no meio acadêmico com produção de conhecimento, ensino ou pesquisa.',
			'ong': 'Trabalho em organizações da sociedade civil, em redes descentralizadas, em ONGs, movimentos sociais, sindicatos e qualquer outra entidade do terceiro setor.',
			'com': 'Trabalho no setor privado.',
			'br': 'Quero colaborar no site como cidadão e não me sinto parte de nenhum dos outros setores listados.'
		};
		var s = e.target.id;
		ctrl.vm.sectorPopup(s);
		ctrl.vm.sectorDescriptionPopup(msg[s]);
		ctrl.vm.popupTagOpened = true;
	},

	sectorClick: function(sector) {
		EditAccount.vm.popupTagOpened = false;
		EditAccount.vm.sector(sector);
		[].forEach.call(document.getElementsByClassName('sector'), function(e) {
			e.classList.remove('active');
		});
		setTimeout(function() {
			document.getElementById(sector).classList.add('active');
		}, 500);
	},
	states: m.prop([{
		id: '',
		name: 'Carregando'
	}]),
	selectedState: m.prop(""),
	selectedCitie: m.prop(""),
	statesDisabled: true,
	cities: m.prop(['Escolha um estado antes']),
	citiesDisabled: true,
	changeState: function(state) {
		EditAccount.vm.selectedState(state);
		m.request({
			method: 'GET',
			url: './json/' + state.toLowerCase() + '.json'
		}).then(function(data) {
			data.unshift('Cidade');
			EditAccount.vm.selectedCitie(EditAccount.user().cidade);
			EditAccount.vm.cities(data);
			EditAccount.vm.citiesDisabled = false;
		});
	},
};

EditAccount.view = function(ctrl) {
	return m('.preferences.pure-form',
		m('.container.edit_profile', [
			m('form.pure-form[name=signup]', { onsubmit: ctrl.save.bind(ctrl) }, [

				main_form(ctrl),

				m('button.pure-button.right-content', {
					type: "submit"
				}, 'Salvar'),
				// m('a.pure-button.cancelar[href=/meu-perfil]', {
				//     config: m.route
				// }, 'Voltar')
			])
		]),
		m('hr.full-line-separator'),
		m('.container.privacy.box-config', [
			m('h2', [
				m('i.icon-privacidade'),
				'privacidade'
			]),
			m('.pure-u-1', [
				m('label.pure-radio', [
					m('input[type=radio][name=privacy][value=public]['+(ctrl.isPrivate() ? '' : 'checked')+']', {onchange: ctrl.save_config.bind(this, ctrl)}),
					m('strong', 'PERFIL PÚBLICO:'),
					'OUTROS USUÁRIOS PODEM VER MINHAS INFORMAÇÕES PESSOAIS (LOCALIZAÇÃO, PROFISSÃO E “SOBRE MIM”) E EMAIL DE CONTATO.'
				]),
				m('label.pure-radio', [
					m('input[type=radio][name=privacy][value=private]['+(ctrl.isPrivate() ? 'checked' : '')+']', {onchange: ctrl.save_config.bind(this, ctrl)}),
					m('strong', 'PERFIL PRIVADO:'),
					'OUTROS USUÁRIOS NÃO PODEM VER MINHAS INFORMAÇÕES PESSOAIS (LOCALIZAÇÃO, PROFISSÃO E “SOBRE MIM”) E EMAIL DE CONTATO.'
				])
			])
		]),

		m('hr.full-line-separator.hidden'),
		m('.container.notifications.box-config.hidden', [
			m('h2', [
				m('i.icon-configuracoes'),
				'notificações'
			]),

			m('table.pure-table',[
				m('thead', [
					m('tr', [
						m('th.notification', ''),
						m('th', 'Por email'),
						m('th', 'No celular')
					])
				]),
				m('tbody', [
					m('tr', [
						m('td', 'Alguém comentou na minha discussão'),
						m('td', m('input[type=checkbox][id=email_my_discussion][value=ativo][checked]')),
						m('td', m('input[type=checkbox][id=celular_my_discussion][value=ativo][checked]')),
					]),
					m('tr', [
						m('td', 'Alguém gostou da minha discussão'),
						m('td', m('input[type=checkbox][id=email_my_discussion][value=ativo][checked]')),
						m('td', m('input[type=checkbox][id=celular_my_discussion][value=ativo][checked]')),
					]),
					m('tr', [
						m('td', 'Alguém não gostou da minha discussão'),
						m('td', m('input[type=checkbox][id=email_my_discussion][value=ativo][checked]')),
						m('td', m('input[type=checkbox][id=celular_my_discussion][value=ativo][checked]')),
					]),
					m('tr', [
						m('td', 'Alguém gostou do meu comentário'),
						m('td', m('input[type=checkbox][id=email_my_discussion][value=ativo][checked]')),
						m('td', m('input[type=checkbox][id=celular_my_discussion][value=ativo][checked]')),
					]),
					m('tr', [
						m('td', 'Alguém não gostou do meu comentário'),
						m('td', m('input[type=checkbox][id=email_my_discussion][value=ativo][checked]')),
						m('td', m('input[type=checkbox][id=celular_my_discussion][value=ativo][checked]')),
					])
				])

			])
		]),
		m('hr.full-line-separator'),
		m('.container.password-change.box-config', [
			m('h2', [
				m('i.icon-cadeado-fechado'),
				'alterar senha'
			]),
			m('.pure-u-sm-6-24.pure-u-1', [
				m('input[type=password][name=current_password][placeholder=Senha atual]')
			]),
			m('.pure-u-sm-6-24.pure-u-1', [
				m('input[type=password][name=new_password][placeholder=Nova senha]')
			]),
			m('.pure-u-sm-6-24.pure-u-1', [
				m('input[type=password][name=repeat_new_password][placeholder=Repetir nova senha]')
			]),
			m('.pure-u-sm-6-24.pure-u-1', [
				m('a.pure-button.pure-input-1[href=#]', 'salvar')
			])
		])
	);
};

module.exports = EditAccount;
