var m = require('mithril');
var profile = {};
var badge_user = require('views/common/badge_user');
var userModel = require('models/user');
var configModel = require('models/config');

profile.controller = function () {
    var user_id = m.route.param('id');
    var ctrl = this;
    ctrl.model = m.prop({});
    userModel.get_by_id(user_id).then(function (data) {
        ctrl.model(data);
    });
};

profile.view = function (ctrl) {
    var model = ctrl.model();
    return [
        m('.public-profile-page.container', 
            m('a.pure-button.cancelar[href=#]', {onclick: function (e) { window.history.back(); return false; }}, 'voltar'),
            m('.pure-g', [
                m('.pure-u-1.pure-u-sm-8-24', [
                    m('.profile-tag-big',
                        badge_user(model, 200)
                    )
                ]),
                m('.pure-u-1.pure-u-sm-10-24', [
                    m('.profile-details',
                        m('.pure-u-1-5', m('.separator')),
                        m('.pure-u-4-5', [
                            m('h1', model.nome),
                            m('h3', model.email),
                            m('h3', model.ocupacao),
                            m('h3', model.cidade + ' - ' + model.estado),
                            m('p', model.descricao)
                        ])
                    )
                ]),
            ])
         )
    ];
};

module.exports = profile;
