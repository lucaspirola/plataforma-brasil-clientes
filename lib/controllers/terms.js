var m = require('mithril');
var creditos_foto = require('views/common/creditos_foto');
var configModel = require('models/config');
var md2html = require('markdown2mithril');

function controller () {
    var desc = m.prop('');
    configModel.get_site('page_termos').then(function(data) {
        desc(data);  
    });

    return {
        content : function () {
            return md2html(desc());
        }
    };
}

function view(ctrl) {
    return m('.page', [
        m('.hero', [
            m('. title.pure-u-4-5.pure-u-sm-3-5', [
                m('h1', 'Termos de uso')
            ]),
            creditos_foto()
        ]),
        m('.container', [
            m('.pure-g', [
                m('.pure-u-1.content', [
                    ctrl.content(),
                ])
            ])
        ])
    ]);
}

module.exports = {
    controller: controller,
    view: view
};
