var m = require('mithril');
var c = require('config');
var userModel = require('models/user');

var notifications = require('views/profile/notifications.js');

var Vote = module.exports = {
    controller: function() {
        var ctrl = this;
        ctrl.total_notifications = m.prop(0);
        ctrl.current_user = m.prop(userModel.isAuthenticate(localStorage.user));
		userModel.
			get_notifications(ctrl.current_user).
			then(function (data) {
				var d = [];
				Array.prototype.push.apply(d, data[0]);
				Array.prototype.push.apply(d, data[1]);
				ctrl.model = m.prop(d);
			});
        userModel.get_total_notifications(ctrl.current_user).then(function (total) {
            ctrl.total_notifications(total.total);
        });
    },

    view: function(ctrl) {
        return [
            m('.notifications.container',
                notifications(ctrl)
            )
        ];
    }
};
