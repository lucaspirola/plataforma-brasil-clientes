var m = require('mithril');
var discussionModel = require('models/discussion');
var commentModel = require('models/comment');
var tooltip = require('views/common/tooltip');
var share_fb_dialog = require('views/common/share_fb_dialog');

var social = {

	controller: function () {
		var ctrl = this;

		ctrl.like = function(ctrl, e) {
            return false; // disable because phase 3
			// var data = ctrl.data();
			// if (data.discussion_id === undefined) {
			// 	discussionModel.like(data.id);
			// } else {
			// 	commentModel.like(data.discussion_id, data.id);
			// }
			// data.my_likes = 1;
			// data.likes = parseInt(data.likes) + 1;
			// return false;
		};

		ctrl.dislike = function (ctrl, e) {
            return false; // disable because phase 3
			// var data = ctrl.data();
			// if (data.discussion_id === undefined) {
			// 	discussionModel.dislike(data.id);
			// } else {
			// 	commentModel.dislike(data.discussion_id, data.id);
			// }
			// data.my_dislikes = 1;
			// data.dislikes = parseInt(data.dislikes) + 1;
			// return false;
		};

		ctrl.share_facebook = function (ctrl, e) {
			var data = ctrl.data();
			if (data.discussion_id === undefined) {
				share_fb_dialog(
					data.description,
					'plataformabrasil.org.br ',
					'/?/fase-2-comentar-discussao/'+data.id,
					data.title
				);
			} else {
				share_fb_dialog(
					data.description,
					'plataformabrasil.org.br ',
					'/?/fase-2-comentar-discussao/'+data.discussion_id,
					'Leia o comentário do '+data.user.nome
				);
			}

        	return false;
		};

		ctrl.flag_discussion = function (ctrl, e) {
			var d = ctrl.data();
			var flagged_success = function (data) {
				d.content_tip = {};
				d.content_tip.event = e;
				d.content_tip.content = m( 'p.alert-success', {
									onclick : function(){ 
										d.content_tip = undefined;
									}
								}, 'O Comitê de ética irá examinar o conteúdo e você será avisado.',m('br'), m('i.icon-circle-with-cross') );
				return data;
			};
			var flagged_error = function (err) {
				d.content_tip = {};
				d.content_tip.event = e;
				d.content_tip.content = m( 'p.alert-error', {
									onclick : function(){ 
										d.content_tip = undefined;
									}
								}, err.error + '.', m('i.icon-circle-with-cross') );
			};

			if (d.discussion_id === undefined) {
				discussionModel.flag(d.id)
				.then(flagged_success, flagged_error);
			} else {
				commentModel.flag(d.discussion_id, d.id)
				.then(flagged_success, flagged_error);
			}
			return false;
		};
	},

	view: function (ctrl) {
		var liked = (ctrl.data().my_likes === 1 ? '.selected' : '');
		var disliked = (ctrl.data().my_dislikes === 1 ? '.selected' : '');
		return [
			m('a.action-like'+liked+'[href=#][data-id='+ctrl.data().id+']', {onclick: ctrl.like.bind(this, ctrl)}, [
				m('i.icon-gostei'),
				m('span', ctrl.data().likes)
			]),
			m('a.action-dislike'+disliked+'[href=#][data-id='+ctrl.data().id+']',{onclick: ctrl.dislike.bind(this, ctrl)},[
				m('i.icon-nao-gostei'),
				m('span', ctrl.data().dislikes)
			]),
			m('a.facebook.action-share.secondary[href=#][data-id='+ctrl.data().id+'][title=compartilhar no facebook]',
				{onclick: ctrl.share_facebook.bind(this, ctrl)},
				m('i.icon-facebook')
			),
			m('a.action-flag.secondary[href=#][title=Denuncie essa mensagem como ofensiva.][data-id='+ctrl.data().id+']', 
				{onclick: ctrl.flag_discussion.bind(this,ctrl)}, [
				m('i.icon-warning')
			]),
			(ctrl.data().content_tip !== undefined ? tooltip(ctrl.data().content_tip) : '')
		];
	}
};


module.exports = social;
