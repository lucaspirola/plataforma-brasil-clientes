var m = require('mithril');
var c = require('config');
var _ = require('underscore');

var userModel = require('models/user');
var discussionModel = require('models/discussion');
var commentModel = require('models/comment');
var comments_page = require('views/comments');

var Comments = module.exports = {
    controller: function() {
        var ctrl = this;

		ctrl.discussion_id = m.prop(m.route.param("id"));
        ctrl.comment_id = m.prop(m.route.param("comentario_id"));
        ctrl.featured_discussion = m.prop(discussionModel.get(ctrl.discussion_id()));
        commentModel.get_all_by_discussion(ctrl.discussion_id()).then(function (data) {
            ctrl.comments = m.prop(data);
            ctrl.total_records = m.prop(commentModel.total_records());
        });
        
        ctrl.is_user_loggedin = m.prop(false);
		ctrl.current_user = m.prop({});

		if (localStorage.token !== undefined) {
			userModel.getUser().then(ctrl.current_user);
			ctrl.is_user_loggedin(true);
		}

		ctrl.model = {
			description: m.prop(''),
			message: m.prop([])
		};

		ctrl.save_comment = commentModel.save_comment;
        ctrl.get_more_comments = commentModel.get_more_comments;

        var leu_notificacao = m.route.param("leu_notificacao");
        if (leu_notificacao !== undefined) {
            discussionModel.notification_read(m.route.param("leu_notificacao"));
            if (ctrl.comment_id() > 0) {
                commentModel.notification_read(m.route.param("leu_notificacao"));
            }
        }
    },

    view: function(ctrl) {
        return [
            m('.comments-page',
                comments_page(ctrl)
            )
        ];
    }
};
