// phase 1 actions
var vote = require('controllers/vote');
var results = require('controllers/results');
var my_results = require('controllers/my_results');

// cms page
var home = require('controllers/home');
var about = require('controllers/about');
var privacy = require('controllers/privacy');
var terms = require('controllers/terms');
var ombudsman = require('controllers/ombudsman');
var contato = require('controllers/contato');

// users signup related
var signup = require('controllers/signup');
var login = require('controllers/login');
var reset_password = require('controllers/reset_password');

// phase 2 actions
var discussions = require('controllers/discussions');
var comments = require('controllers/comments');
var handyman = require('controllers/handyman');

// user profile actions
var profile = require('controllers/profile');
var preferences = require('controllers/preferences');
var notifications = require('controllers/notifications');
var my_answers = require('controllers/my_answers');
var activity = require('controllers/activity');
var logout = require('controllers/logout');

// report actions
var report_phase_1 = require('controllers/report_phase_1');
var report_phase_2 = require('controllers/report_phase_2');
var report_phase_3 = require('controllers/report_phase_3');

// download pages
var download_completo = {
	controller: function () {
		this.report_phase_3_partial = new report_phase_3.controller();
	},
	view: function (ctrl) {
        return report_phase_3.view(ctrl.report_phase_3_partial);
	}
};


var m = require('mithril');
var routes  = {};

routes.init = function () {
    m.route(document.getElementById('main'), '/', {
        '/fase-1-votar': vote,
        '/fase-1-resultados': results,
        '/fase-1-meus-resultados': my_results,

		'/': home,
        '/sobre': about,
        '/politica-de-privacidade': privacy,
        '/termos-de-uso': terms,
        '/criticas-e-sugestoes': ombudsman,
        '/contato': contato,

		'/abrir-conta/:access_token': signup,
        '/entrar': login,
        '/nova-senha/:reset_code': reset_password,

        '/fase-2-discutir-tema/:id': discussions,
        '/fase-2-comentar-discussao/:id': comments,
        '/fase-2-informar-se-discussao/:id': handyman,

        '/comentarios-fase-1': report_phase_1,
        '/comentarios-fase-2': report_phase_2,
        '/relatorios-fase-3': report_phase_3,

		'/perfil/:id': profile,
        '/minhas-preferencias': preferences,
        '/notificacoes': notifications,
        '/minhas-respostas': my_answers,
        '/atividades': activity,
        '/sair': logout,
		'/download-relatorio-completo': download_completo,
		'/download-relatorio-tema-1': download_completo,
		'/download-relatorio-tema_2': download_completo,
		'/download-relatorio-team-3': download_completo,
		'/download-relatorio-tema-4': download_completo,
		'/download-relatorio-tema-5': download_completo
    });
};

module.exports = routes;
