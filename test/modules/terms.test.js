/*jshint expr: true */
/*global describe, it */

var should = require('should');
var terms = require('../../lib/modules/terms');

describe('terms', function () {

  describe('controller', function () {

    it('should be a function', function () {
      terms.controller.should.be.a.function;
    });

    it('should return an object', function () {
      terms.controller().should.be.a.object;
    });

  });

  describe('view', function () {

    it('should be a function', function () {
      terms.view.should.be.a.function;
    });

    it('should return an object', function () {
      terms.view().should.be.a.object;
    });

  });

});
