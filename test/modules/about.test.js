/*jshint expr: true */
/*global describe, it */

var should = require('should');
var about = require('../../lib/modules/about');

describe('about', function () {

  describe('controller', function () {

    it('should be a function', function () {
      about.controller.should.be.a.function;
    });

    it('should return an object', function () {
      about.controller().should.be.a.object;
    });

  });

  describe('view', function () {

    it('should be a function', function () {
      about.view.should.be.a.function;
    });

    it('should return an object', function () {
      about.view().should.be.a.object;
    });

  });

});
