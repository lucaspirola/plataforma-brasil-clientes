/*jshint expr: true */
/*global describe, it */

var should = require('should');
var activity = require('../../lib/modules/activity');

describe('activity', function () {

  describe('controller', function () {

    it('should be a function', function () {
      activity.controller.should.be.a.function;
    });

    it('should return an object', function () {
      activity.controller().should.be.a.object;
    });

  });

  describe('view', function () {

    it('should be a function', function () {
      activity.view.should.be.a.function;
    });

    it('should return an object', function () {
      activity.view().should.be.a.object;
    });

  });

});
