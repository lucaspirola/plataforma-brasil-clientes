/*jshint expr: true */
/*global describe, it */

var should = require('should');
var signup = require('../../lib/modules/signup');

describe('signup', function () {

  describe('controller', function () {

    it('should be a function', function () {
      signup.controller.should.be.a.function;
    });
/*
    it('should return an object', function () {
      signup.controller().should.be.a.object;
    });
*/
  });

  describe('view', function () {

    it('should be a function', function () {
      signup.view.should.be.a.function;
    });
/*
    it('should return an object', function () {
      signup.view().should.be.a.object;
    });
*/
  });

});
