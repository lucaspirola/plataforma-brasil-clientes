/*jshint expr: true */
/*global describe, it */

var should = require('should');
var account = require('../../lib/modules/account');

describe('account', function () {

  describe('controller', function () {

    it('should be a function', function () {
      account.controller.should.be.a.function;
    });

    it('should return an object', function () {
      account.controller().should.be.a.object;
    });

  });

  describe('view', function () {

    it('should be a function', function () {
      account.view.should.be.a.function;
    });

    it('should return an object', function () {
      account.view().should.be.a.object;
    });

  });

});
