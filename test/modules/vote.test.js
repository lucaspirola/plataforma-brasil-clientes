/*jshint expr: true */
/*global describe, it */

var should = require('should');
var vote = require('../../lib/modules/vote');

describe('vote', function () {

  describe('controller', function () {

    it('should be a function', function () {
      vote.controller.should.be.a.function;
    });
/*
    it('should return an object', function () {
      vote.controller().should.be.a.object;
    });
*/
  });

  describe('view', function () {

    it('should be a function', function () {
      vote.view.should.be.a.function;
    });
/*
    it('should return an object', function () {
      vote.view().should.be.a.object;
    });
*/
  });

});
