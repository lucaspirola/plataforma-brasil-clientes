# Plataforma Brasil Site
  [ ![Codeship Status for nucleo-digital/plataforma-brasil-clients](https://codeship.com/projects/03be1e70-e7b9-0132-ab4d-4e340869c11f/status?branch=master)](https://codeship.com/projects/82620)
  [![Code Climate](https://codeclimate.com/github/nucleo-digital/plataforma-brasil-clients/badges/gpa.svg)](https://codeclimate.com/github/nucleo-digital/plataforma-brasil-clients)
  [![Test Coverage](https://codeclimate.com/github/nucleo-digital/plataforma-brasil-clients/badges/coverage.svg)](https://codeclimate.com/github/nucleo-digital/plataforma-brasil-clients)

## Getting Started

```
git clone git@github.com:nucleo-digital/plataforma-brasil-clients.git
cd plataforma-brasil-clients
npm install
./node_modules/bower/bin/bower install
npm start
```
